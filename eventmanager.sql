-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Loomise aeg: Aprill 19, 2021 kell 02:44 PL
-- Serveri versioon: 10.4.11-MariaDB
-- PHP versioon: 7.2.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Andmebaas: `eventmanager`
--

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `about`
--

CREATE TABLE IF NOT EXISTS `about` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `language` varchar(255) DEFAULT NULL,
  `is_about` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `event_id` bigint(20) DEFAULT NULL,
  `venue_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKrwj1pw2sfhrm1fgrv08nv5a5d` (`event_id`),
  KEY `FKee2rp9ilpky4yp9s6oeij1p8q` (`venue_id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4;

--
-- Andmete tõmmistamine tabelile `about`
--

INSERT IGNORE INTO `about` (`id`, `name`, `description`, `language`, `is_about`, `parent_id`, `event_id`, `venue_id`) VALUES
(1, 'Näidissündmus näitamaks sündmustehalduri omadusi', 'See siin on väljamõeldud sündmus, mis näitab seda, mismoodi on ühe sündmusega seotud andmed omavahel seotud ning kuidas nad paistavad välja eeskätt sellele inimesele, kes sündmust haldab. \r\n\r\nKuna see on loodud mitmekeelsena, siis on siin ühe sündmuse kohta kolm tekstilist kannet, kõik erinevates keeltes. Tavakasutaja näeb ainult üht korraga - selles keeles, milles ta tahab endale teavet kuvada. Samuti peaks tavakasutaja nägema sündmuse andmestikku veidi \"inimlikumal\" kujul, mitte sellises töölaua vormis. \r\n\r\nHetkel saab siin hallata vaid nö tavalisi sündmusi ehk teisisõnu üksiksündmusi, mis võivad korduda ja toimuda erinevates kohtades. Üksiksündmus tähendab seda, et tegemist ei ole suure festivali laadse üritusega, millel on palju alamsündmusi, ning seega ei saa olla ka tegu ühegi sündmuse alamsündmusega. Küll aga on põhimõtteliselt võimalik luua lihtsamat, nö konverentsi-tüüpi sündmusi, kus iga toimumiskord (ettekannne jne) võib olla eraldi kirjeldatav. Seda võimaldab igale toimumiskorrale ette nähtud võimalus mitmekeelse lisateabe jaoks, mis võib olla nii teave ettekandega esineja kohta kui ka näiteks sündmuse või selle toimumiskorra muudatuse kohta. \r\n\r\nKuna kuupäeva- ja kellaaja väljad on andmebaasis eraldi, siis lihtsustab see sündmuse andmete kuvamist nö loomulikul viisil. Näiteks ühel päeval kellast kellani toimuv üritus toimuki sel päeval sellest kellast selle kellani, mitte selle päeva sellest kellast selle(sama) päeva selle kellani. Määramata kellaaegadega nn kogupäevaüritus toimub sel kuupäeval, mitte sellest kuupäevast sellesama kuupäevani. \r\n\r\nSündmuse andmete juures on oma koht mõistagi korraldajate infoga. Lisavõimalus on sündmus osalejate registreerimisega. Kommenteerimmise võimalus on ka ette nähtud, kuid see ei pruugi esitluse ajaks olemas olla. ', 'et', 'EVENT', 1, 1, NULL),
(2, 'La place pour l\'évenement', 'Celui-ci est la déscription de l\'endroit ou les choses se passent', 'fr', 'VENUE', 1, NULL, 1),
(3, 'Events venue', 'Here we describe where actually people should go if they go to the event', 'en', 'VENUE', 1, NULL, 1),
(4, 'See on koht, kus toimub asju', 'Eks seda kohta, kus midagi aset leiab, peab kirjeldama ka, kui just rahvast üllatada ei taha', 'et', 'VENUE', 1, NULL, 1),
(5, 'Another place', 'Another description of another place', 'en', 'VENUE', 2, NULL, 2),
(6, 'Et voilà l\'évenement', 'Rien d\'écrire', 'fr', 'EVENT', 2, 2, NULL),
(7, 'Exemple d\'événement pour afficher les propriétés du gestionnaire d\'événements', 'Il s\'agit d\'un événement fictif, qui montre comment les données liées à un événement sont liées les unes aux autres et à quoi elles ressemblent, en particulier pour la personne qui gère l\'événement.\r\n\r\nParce qu\'il est multilingue, il y a trois entrées textuelles pour un événement, toutes dans des langues différentes. L\'utilisateur moyen n\'en voit qu\'un à la fois - dans la langue dans laquelle il souhaite afficher les informations. En outre, l\'utilisateur moyen devrait voir les données d\'événement sous une forme légèrement plus «humaine», plutôt que dans un tel format de bureau.\r\n\r\nPour le moment, seuls les événements dits ordinaires peuvent être gérés ici, c\'est-à-dire des événements individuels qui peuvent être répétés et se dérouler à différents endroits. Un seul événement signifie qu\'il ne s\'agit pas d\'un grand événement de type festival avec de nombreux sous-événements, et ne peut donc être un sous-événement d\'aucun événement. Cependant, en principe, il est possible de créer des événements de type conférence plus simples, où chaque procédure (présentation, etc.) peut être décrite séparément. Ceci est rendu possible par la possibilité offerte pour chaque lieu pour des informations complémentaires multilingues, qui peuvent être à la fois des informations sur le présentateur et, par exemple, un événement ou un changement de lieu.\r\n\r\nLes champs de date et d\'heure étant séparés dans la base de données, cela simplifie l\'affichage des données d\'événement d\'une manière dite naturelle. Par exemple, un événement qui aura lieu ce jour-là de cette heure à cette heure, sera affiché justement comme ca, pas de ce jour de cette heure à ce (même) jour à cette heure. L\'événement dit de la journée totale avec des heures indéfinies a lieu à cette date, et non à partir de cette date jusqu\'à la même date.\r\n\r\nBien entendu, les informations sur les organisateurs ont leur place à côté des données de l\'événement. Une option supplémentaire est un événement avec l\'inscription des participants. La possibilité de commenter est également fournie, mais elle peut ne pas être disponible pendant la présentation.', 'fr', 'EVENT', 1, 1, NULL),
(8, 'Sample event to show event manager properties', 'This is a fictional event, which shows how the data related to one event is related to each other and how it looks, especially to the person who manages the event.\r\n\r\nBecause it is multilingual, there are three textual entries for one event, all in different languages. The average user sees only one at a time - in the language in which he wants to display the information. Also, the average user should see the event data in a slightly more \"human\" form, rather than in such a desktop format.\r\n\r\nAt the moment, only so-called ordinary events can be managed here, in other words, individual events that can be repeated and take place in different places. A single event means that it is not a large festival-like event with many sub-events, and therefore cannot be a sub-event of any event. However, in principle, it is possible to create simpler, so-called conference-type events, where each procedure (presentation, etc.) can be described separately. This is made possible by the possibility provided for each venue for multilingual additional information, which can be both information about the presenter and, for example, an event or a change in its venue.\r\n\r\nAs the date and time fields are separate in the database, this simplifies the display of event data in a so-called natural way. For example, an event that will take place on that day from that time to that time, will displayed exactly in this mannere and not from that day from that time to that (same) day to that time. The so-called total day event with indefinite times takes place on that date, not from that date until the same date.\r\n\r\nOf course, information about the organizers has its place next to the event data. An additional option is an event with the registration of participants. The opportunity to comment is also provided, but it may not be available during the presentation.', 'en', 'EVENT', 1, 1, NULL),
(9, 'Teine koht', 'Teine kirjeldus, mis käib teise koha kohta', 'et', 'VENUE', 2, NULL, 2),
(10, 'Un autre place', 'et une autre texte à décriver', 'fr', 'VENUE', 2, NULL, 2),
(11, 'Another event', 'Another description for another event', 'en', 'EVENT', 2, 2, NULL),
(12, 'Teine sündmus', 'Ja siin on hoopis teistsugune kirjeldus', 'et', 'EVENT', 2, 2, NULL),
(13, 'Absoluutselt teistsugune koht', 'Avastasin, et kolmanda paiga kohta polnud ühtki kirjeldust', 'et', 'VENUE', 3, NULL, 3),
(14, 'Absolutely another place', 'Without textual information about the venue it just doesn\'t display', 'en', 'VENUE', 3, NULL, 3),
(15, 'Une place absolument différent', 'Sans une texte decrivant la place on n\'en peut rien savoier :)', 'fr', 'VENUE', 3, NULL, 3),
(16, 'Näidissündmus näitamaks uue sündmuse sisestamist', 'See siin on väljamõeldud sündmus, mis näitab seda, mismoodi on ühe sündmusega seotud andmed omavahel seotud ning kuidas nad paistavad välja eeskätt sellele inimesele, kes sündmust haldab. ', 'et', 'EVENT', NULL, 7, NULL),
(17, 'Exemple d\'événement pour afficher la soumission d\'un nouvel evenement', 'Il s\'agit d\'un événement fictif, qui montre comment les données liées à un événement sont liées les unes aux autres et à quoi elles ressemblent, en particulier pour la personne qui gère l\'événement.', 'fr', 'EVENT', NULL, 7, NULL),
(18, 'Sample event to show new event submission', 'This is a fictional event, which shows how the data related to one event is related to each other and how it looks, especially to the person who manages the event.', 'en', 'EVENT', NULL, 7, NULL),
(19, 'Näidissündmus näitamaks uue sündmuse sisestamist', 'See siin on väljamõeldud sündmus, mis näitab seda, mismoodi on ühe sündmusega seotud andmed omavahel seotud ning kuidas nad paistavad välja eeskätt sellele inimesele, kes sündmust haldab. ', 'et', 'EVENT', NULL, 8, NULL),
(20, 'Exemple d\'événement pour afficher la soumission d\'un nouvel evenement', 'Il s\'agit d\'un événement fictif, qui montre comment les données liées à un événement sont liées les unes aux autres et à quoi elles ressemblent, en particulier pour la personne qui gère l\'événement.', 'fr', 'EVENT', NULL, 8, NULL),
(21, 'Sample event to show new event submission', 'This is a fictional event, which shows how the data related to one event is related to each other and how it looks, especially to the person who manages the event.', 'en', 'EVENT', NULL, 8, NULL),
(22, 'Näidissündmus näitamaks uue sündmuse sisestamist', 'See siin on väljamõeldud sündmus, mis näitab seda, mismoodi on ühe sündmusega seotud andmed omavahel seotud ning kuidas nad paistavad välja eeskätt sellele inimesele, kes sündmust haldab. ', 'et', 'EVENT', NULL, 9, NULL),
(23, 'Exemple d\'événement pour afficher la soumission d\'un nouvel evenement', 'Il s\'agit d\'un événement fictif, qui montre comment les données liées à un événement sont liées les unes aux autres et à quoi elles ressemblent, en particulier pour la personne qui gère l\'événement.', 'fr', 'EVENT', NULL, 9, NULL),
(24, 'Sample event to show new event submission', 'This is a fictional event, which shows how the data related to one event is related to each other and how it looks, especially to the person who manages the event.', 'en', 'EVENT', NULL, 9, NULL),
(25, 'See sündmus on väheke isevärki', 'Annab ikka välja mõelda neid näidissündmusi, mis mõeldud üksnes näitamaks, kuidas nad paistavad välja eeskätt sellele inimesele, kes sündmust haldab. ', 'et', 'EVENT', NULL, 10, NULL),
(26, 'Cet évenement est un peu particulière', 'Il s\'agit d\'un événement fictif, qui montre comment les données liées à un événement sont liées les unes aux autres et à quoi elles ressemblent, en particulier pour la personne qui gère l\'événement.', 'fr', 'EVENT', NULL, 10, NULL),
(27, 'This event is a little bit particulary', 'This is a fictional event, which shows how the data related to one event is related to each other and how it looks, especially to the person who manages the event.', 'en', 'EVENT', NULL, 10, NULL),
(28, 'See väheke isevärki', 'Annab ikka välja mõelda neid näidissündmusi, mis mõeldud üksnes näitamaks, kuidas nad paistavad välja eeskätt sellele inimesele, kes sündmust haldab. ', 'et', 'EVENT', NULL, 11, NULL),
(29, ' un peu particulière', 'Il s\'agit d\'un événement fictif, qui montre comment les données liées à un événement sont liées les unes aux autres et à quoi elles ressemblent, en particulier pour la personne qui gère l\'événement.', 'fr', 'EVENT', NULL, 11, NULL),
(30, 'a little bit particulary', 'This is a fictional event, which shows how the data related to one event is related to each other and how it looks, especially to the person who manages the event.', 'en', 'EVENT', NULL, 11, NULL);

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `address`
--

CREATE TABLE IF NOT EXISTS `address` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `street_name` varchar(255) DEFAULT NULL,
  `house_number` varchar(255) DEFAULT NULL,
  `app_number` varchar(255) DEFAULT NULL,
  `postal_code` varchar(255) DEFAULT NULL,
  `territorial_unit_id` int(11) DEFAULT NULL,
  `country_id` bigint(20) DEFAULT NULL,
  `city_or_village_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK6wkhgog5i9mfnacf6walvnt1n` (`city_or_village_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;

--
-- Andmete tõmmistamine tabelile `address`
--

INSERT IGNORE INTO `address` (`id`, `street_name`, `house_number`, `app_number`, `postal_code`, `territorial_unit_id`, `country_id`, `city_or_village_id`) VALUES
(1, 'Lai', '1', '1', '44306', NULL, 233, 1),
(2, 'Fr.R.Kreutzwaldi', '2', '', '44314', NULL, 233, 1),
(3, 'Harku', '2', '5', '11611', NULL, 233, 2),
(4, 'Gonsiori', '21', '', '10147', NULL, 233, 2),
(5, 'Turu plats', '2', '', '11614 ', NULL, 233, 2),
(6, 'Sipes Neck', 'morph USB', 'projection Taka JBOD', 'Accounts blue invoice', NULL, 233, NULL),
(7, 'Jolie Lakes', 'Awesome Industrial', 'input Account', 'Cheese Account Tasty', NULL, 233, NULL),
(8, 'Bogisich Islands', 'Ramp benchmark Rubber', 'payment fuchsia context-sensitive', 'Cameroon Pizza Grass-roots', NULL, 233, NULL),
(9, 'Dagmar Stream', 'Soft', 'systematic Avon analyzer', 'regional Regional withdrawal', NULL, 233, NULL),
(10, 'Sigurd Garden', 'USB implement', 'CSS compress', 'copying', NULL, 233, NULL),
(11, 'Pikk', '10', NULL, '44307', 1, 233, 1);

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `comment`
--

CREATE TABLE IF NOT EXISTS `comment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `comment_text` varchar(255) DEFAULT NULL,
  `language` varchar(255) DEFAULT NULL,
  `event_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKhr48nopy5aorw0ta1ii704tpu` (`event_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

--
-- Andmete tõmmistamine tabelile `comment`
--

INSERT IGNORE INTO `comment` (`id`, `comment_text`, `language`, `event_id`) VALUES
(1, 'payment Hawaii Health', 'fr', NULL),
(2, 'monitor', 'et', NULL),
(3, 'Refined fault-tolerant 1080p', 'fr', NULL),
(4, 'Liberia Fresh cross-platform', 'fr', NULL),
(5, 'Sausages', 'et', NULL),
(6, 'indigo synthesize', 'fr', NULL),
(7, 'Savings feed', 'en', NULL),
(8, 'online system-worthy eyeballs', 'et', NULL),
(9, 'e-business Garden', 'fr', NULL),
(10, 'state Unbranded Springs', 'en', NULL);

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `country`
--

CREATE TABLE IF NOT EXISTS `country` (
  `id` smallint(3) UNSIGNED ZEROFILL NOT NULL,
  `iso_2` varchar(255) DEFAULT NULL,
  `short_name` varchar(255) DEFAULT NULL,
  `long_name` varchar(255) DEFAULT NULL,
  `iso_3` varchar(255) DEFAULT NULL,
  `numcode` varchar(255) DEFAULT NULL,
  `un_member` varchar(255) DEFAULT NULL,
  `calling_code` varchar(255) DEFAULT NULL,
  `cctld` varchar(255) DEFAULT NULL,
  `region_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_country__region_id` (`region_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Andmete tõmmistamine tabelile `country`
--

INSERT IGNORE INTO `country` (`id`, `iso_2`, `short_name`, `long_name`, `iso_3`, `numcode`, `un_member`, `calling_code`, `cctld`, `region_id`) VALUES
(000, 'XK', 'Kosovo', 'Republic of Kosovo', 'XKX', NULL, 'some', '381', NULL, NULL),
(004, 'AF', 'Afghanistan', 'Islamic Republic of Afghanistan', 'AFG', '004', 'yes', '93', '.af', NULL),
(008, 'AL', 'Albania', 'Republic of Albania', 'ALB', '008', 'yes', '355', '.al', NULL),
(010, 'AQ', 'Antarctica', 'Antarctica', 'ATA', '010', 'no', '672', '.aq', NULL),
(012, 'DZ', 'Algeria', 'People\'s Democratic Republic of Algeria', 'DZA', '012', 'yes', '213', '.dz', NULL),
(016, 'AS', 'American Samoa', 'American Samoa', 'ASM', '016', 'no', '1+684', '.as', NULL),
(020, 'AD', 'Andorra', 'Principality of Andorra', 'AND', '020', 'yes', '376', '.ad', NULL),
(024, 'AO', 'Angola', 'Republic of Angola', 'AGO', '024', 'yes', '244', '.ao', NULL),
(028, 'AG', 'Antigua and Barbuda', 'Antigua and Barbuda', 'ATG', '028', 'yes', '1+268', '.ag', NULL),
(031, 'AZ', 'Azerbaijan', 'Republic of Azerbaijan', 'AZE', '031', 'yes', '994', '.az', NULL),
(032, 'AR', 'Argentina', 'Argentine Republic', 'ARG', '032', 'yes', '54', '.ar', NULL),
(036, 'AU', 'Australia', 'Commonwealth of Australia', 'AUS', '036', 'yes', '61', '.au', NULL),
(040, 'AT', 'Austria', 'Republic of Austria', 'AUT', '040', 'yes', '43', '.at', NULL),
(044, 'BS', 'Bahamas', 'Commonwealth of The Bahamas', 'BHS', '044', 'yes', '1+242', '.bs', NULL),
(048, 'BH', 'Bahrain', 'Kingdom of Bahrain', 'BHR', '048', 'yes', '973', '.bh', NULL),
(050, 'BD', 'Bangladesh', 'People\'s Republic of Bangladesh', 'BGD', '050', 'yes', '880', '.bd', NULL),
(051, 'AM', 'Armenia', 'Republic of Armenia', 'ARM', '051', 'yes', '374', '.am', NULL),
(052, 'BB', 'Barbados', 'Barbados', 'BRB', '052', 'yes', '1+246', '.bb', NULL),
(056, 'BE', 'Belgium', 'Kingdom of Belgium', 'BEL', '056', 'yes', '32', '.be', NULL),
(060, 'BM', 'Bermuda', 'Bermuda Islands', 'BMU', '060', 'no', '1+441', '.bm', NULL),
(064, 'BT', 'Bhutan', 'Kingdom of Bhutan', 'BTN', '064', 'yes', '975', '.bt', NULL),
(068, 'BO', 'Bolivia', 'Plurinational State of Bolivia', 'BOL', '068', 'yes', '591', '.bo', NULL),
(070, 'BA', 'Bosnia and Herzegovina', 'Bosnia and Herzegovina', 'BIH', '070', 'yes', '387', '.ba', NULL),
(072, 'BW', 'Botswana', 'Republic of Botswana', 'BWA', '072', 'yes', '267', '.bw', NULL),
(074, 'BV', 'Bouvet Island', 'Bouvet Island', 'BVT', '074', 'no', 'NONE', '.bv', NULL),
(076, 'BR', 'Brazil', 'Federative Republic of Brazil', 'BRA', '076', 'yes', '55', '.br', NULL),
(084, 'BZ', 'Belize', 'Belize', 'BLZ', '084', 'yes', '501', '.bz', NULL),
(086, 'IO', 'British Indian Ocean Territory', 'British Indian Ocean Territory', 'IOT', '086', 'no', '246', '.io', NULL),
(090, 'SB', 'Solomon Islands', 'Solomon Islands', 'SLB', '090', 'yes', '677', '.sb', NULL),
(092, 'VG', 'Virgin Islands, British', 'British Virgin Islands', 'VGB', '092', 'no', '1+284', '.vg', NULL),
(096, 'BN', 'Brunei', 'Brunei Darussalam', 'BRN', '096', 'yes', '673', '.bn', NULL),
(100, 'BG', 'Bulgaria', 'Republic of Bulgaria', 'BGR', '100', 'yes', '359', '.bg', NULL),
(104, 'MM', 'Myanmar (Burma)', 'Republic of the Union of Myanmar', 'MMR', '104', 'yes', '95', '.mm', NULL),
(108, 'BI', 'Burundi', 'Republic of Burundi', 'BDI', '108', 'yes', '257', '.bi', NULL),
(112, 'BY', 'Belarus', 'Republic of Belarus', 'BLR', '112', 'yes', '375', '.by', NULL),
(116, 'KH', 'Cambodia', 'Kingdom of Cambodia', 'KHM', '116', 'yes', '855', '.kh', NULL),
(120, 'CM', 'Cameroon', 'Republic of Cameroon', 'CMR', '120', 'yes', '237', '.cm', NULL),
(124, 'CA', 'Canada', 'Canada', 'CAN', '124', 'yes', '1', '.ca', NULL),
(132, 'CV', 'Cape Verde', 'Republic of Cape Verde', 'CPV', '132', 'yes', '238', '.cv', NULL),
(136, 'KY', 'Cayman Islands', 'The Cayman Islands', 'CYM', '136', 'no', '1+345', '.ky', NULL),
(140, 'CF', 'Central African Republic', 'Central African Republic', 'CAF', '140', 'yes', '236', '.cf', NULL),
(144, 'LK', 'Sri Lanka', 'Democratic Socialist Republic of Sri Lanka', 'LKA', '144', 'yes', '94', '.lk', NULL),
(148, 'TD', 'Chad', 'Republic of Chad', 'TCD', '148', 'yes', '235', '.td', NULL),
(152, 'CL', 'Chile', 'Republic of Chile', 'CHL', '152', 'yes', '56', '.cl', NULL),
(156, 'CN', 'China', 'People\'s Republic of China', 'CHN', '156', 'yes', '86', '.cn', NULL),
(158, 'TW', 'Taiwan', 'Republic of China (Taiwan)', 'TWN', '158', 'former', '886', '.tw', NULL),
(162, 'CX', 'Christmas Island', 'Christmas Island', 'CXR', '162', 'no', '61', '.cx', NULL),
(166, 'CC', 'Cocos (Keeling) Islands', 'Cocos (Keeling) Islands', 'CCK', '166', 'no', '61', '.cc', NULL),
(170, 'CO', 'Colombia', 'Republic of Colombia', 'COL', '170', 'yes', '57', '.co', NULL),
(174, 'KM', 'Comoros', 'Union of the Comoros', 'COM', '174', 'yes', '269', '.km', NULL),
(175, 'YT', 'Mayotte', 'Mayotte', 'MYT', '175', 'no', '262', '.yt', NULL),
(178, 'CG', 'Congo', 'Republic of the Congo', 'COG', '178', 'yes', '242', '.cg', NULL),
(180, 'CD', 'Democratic Republic of the Congo', 'Democratic Republic of the Congo', 'COD', '180', 'yes', '243', '.cd', NULL),
(184, 'CK', 'Cook Islands', 'Cook Islands', 'COK', '184', 'some', '682', '.ck', NULL),
(188, 'CR', 'Costa Rica', 'Republic of Costa Rica', 'CRI', '188', 'yes', '506', '.cr', NULL),
(191, 'HR', 'Croatia', 'Republic of Croatia', 'HRV', '191', 'yes', '385', '.hr', NULL),
(192, 'CU', 'Cuba', 'Republic of Cuba', 'CUB', '192', 'yes', '53', '.cu', NULL),
(196, 'CY', 'Cyprus', 'Republic of Cyprus', 'CYP', '196', 'yes', '357', '.cy', NULL),
(203, 'CZ', 'Czech Republic', 'Czech Republic', 'CZE', '203', 'yes', '420', '.cz', NULL),
(204, 'BJ', 'Benin', 'Republic of Benin', 'BEN', '204', 'yes', '229', '.bj', NULL),
(208, 'DK', 'Denmark', 'Kingdom of Denmark', 'DNK', '208', 'yes', '45', '.dk', NULL),
(212, 'DM', 'Dominica', 'Commonwealth of Dominica', 'DMA', '212', 'yes', '1+767', '.dm', NULL),
(214, 'DO', 'Dominican Republic', 'Dominican Republic', 'DOM', '214', 'yes', '1+809, 8', '.do', NULL),
(218, 'EC', 'Ecuador', 'Republic of Ecuador', 'ECU', '218', 'yes', '593', '.ec', NULL),
(222, 'SV', 'El Salvador', 'Republic of El Salvador', 'SLV', '222', 'yes', '503', '.sv', NULL),
(226, 'GQ', 'Equatorial Guinea', 'Republic of Equatorial Guinea', 'GNQ', '226', 'yes', '240', '.gq', NULL),
(231, 'ET', 'Ethiopia', 'Federal Democratic Republic of Ethiopia', 'ETH', '231', 'yes', '251', '.et', NULL),
(232, 'ER', 'Eritrea', 'State of Eritrea', 'ERI', '232', 'yes', '291', '.er', NULL),
(233, 'EE', 'Estonia', 'Republic of Estonia', 'EST', '233', 'yes', '372', '.ee', 1),
(234, 'FO', 'Faroe Islands', 'The Faroe Islands', 'FRO', '234', 'no', '298', '.fo', NULL),
(238, 'FK', 'Falkland Islands (Malvinas)', 'The Falkland Islands (Malvinas)', 'FLK', '238', 'no', '500', '.fk', NULL),
(239, 'GS', 'South Georgia and the South Sandwich Islands', 'South Georgia and the South Sandwich Islands', 'SGS', '239', 'no', '500', '.gs', NULL),
(242, 'FJ', 'Fiji', 'Republic of Fiji', 'FJI', '242', 'yes', '679', '.fj', NULL),
(246, 'FI', 'Finland', 'Republic of Finland', 'FIN', '246', 'yes', '358', '.fi', NULL),
(248, 'AX', 'Aland Islands', '&Aring;land Islands', 'ALA', '248', 'no', '358', '.ax', NULL),
(250, 'FR', 'France', 'French Republic', 'FRA', '250', 'yes', '33', '.fr', NULL),
(254, 'GF', 'French Guiana', 'French Guiana', 'GUF', '254', 'no', '594', '.gf', NULL),
(258, 'PF', 'French Polynesia', 'French Polynesia', 'PYF', '258', 'no', '689', '.pf', NULL),
(260, 'TF', 'French Southern Territories', 'French Southern Territories', 'ATF', '260', 'no', NULL, '.tf', NULL),
(262, 'DJ', 'Djibouti', 'Republic of Djibouti', 'DJI', '262', 'yes', '253', '.dj', NULL),
(266, 'GA', 'Gabon', 'Gabonese Republic', 'GAB', '266', 'yes', '241', '.ga', NULL),
(268, 'GE', 'Georgia', 'Georgia', 'GEO', '268', 'yes', '995', '.ge', NULL),
(270, 'GM', 'Gambia', 'Republic of The Gambia', 'GMB', '270', 'yes', '220', '.gm', NULL),
(275, 'PS', 'Palestine', 'State of Palestine (or Occupied Palestinian Territory)', 'PSE', '275', 'some', '970', '.ps', NULL),
(276, 'DE', 'Germany', 'Federal Republic of Germany', 'DEU', '276', 'yes', '49', '.de', NULL),
(288, 'GH', 'Ghana', 'Republic of Ghana', 'GHA', '288', 'yes', '233', '.gh', NULL),
(292, 'GI', 'Gibraltar', 'Gibraltar', 'GIB', '292', 'no', '350', '.gi', NULL),
(296, 'KI', 'Kiribati', 'Republic of Kiribati', 'KIR', '296', 'yes', '686', '.ki', NULL),
(300, 'GR', 'Greece', 'Hellenic Republic', 'GRC', '300', 'yes', '30', '.gr', NULL),
(304, 'GL', 'Greenland', 'Greenland', 'GRL', '304', 'no', '299', '.gl', NULL),
(308, 'GD', 'Grenada', 'Grenada', 'GRD', '308', 'yes', '1+473', '.gd', NULL),
(312, 'GP', 'Guadaloupe', 'Guadeloupe', 'GLP', '312', 'no', '590', '.gp', NULL),
(316, 'GU', 'Guam', 'Guam', 'GUM', '316', 'no', '1+671', '.gu', NULL),
(320, 'GT', 'Guatemala', 'Republic of Guatemala', 'GTM', '320', 'yes', '502', '.gt', NULL),
(324, 'GN', 'Guinea', 'Republic of Guinea', 'GIN', '324', 'yes', '224', '.gn', NULL),
(328, 'GY', 'Guyana', 'Co-operative Republic of Guyana', 'GUY', '328', 'yes', '592', '.gy', NULL),
(332, 'HT', 'Haiti', 'Republic of Haiti', 'HTI', '332', 'yes', '509', '.ht', NULL),
(334, 'HM', 'Heard Island and McDonald Islands', 'Heard Island and McDonald Islands', 'HMD', '334', 'no', 'NONE', '.hm', NULL),
(336, 'VA', 'Vatican City', 'State of the Vatican City', 'VAT', '336', 'no', '39', '.va', NULL),
(340, 'HN', 'Honduras', 'Republic of Honduras', 'HND', '340', 'yes', '504', '.hn', NULL),
(344, 'HK', 'Hong Kong', 'Hong Kong', 'HKG', '344', 'no', '852', '.hk', NULL),
(348, 'HU', 'Hungary', 'Hungary', 'HUN', '348', 'yes', '36', '.hu', NULL),
(352, 'IS', 'Iceland', 'Republic of Iceland', 'ISL', '352', 'yes', '354', '.is', NULL),
(356, 'IN', 'India', 'Republic of India', 'IND', '356', 'yes', '91', '.in', NULL),
(360, 'ID', 'Indonesia', 'Republic of Indonesia', 'IDN', '360', 'yes', '62', '.id', NULL),
(364, 'IR', 'Iran', 'Islamic Republic of Iran', 'IRN', '364', 'yes', '98', '.ir', NULL),
(368, 'IQ', 'Iraq', 'Republic of Iraq', 'IRQ', '368', 'yes', '964', '.iq', NULL),
(372, 'IE', 'Ireland', 'Ireland', 'IRL', '372', 'yes', '353', '.ie', NULL),
(376, 'IL', 'Israel', 'State of Israel', 'ISR', '376', 'yes', '972', '.il', NULL),
(380, 'IT', 'Italy', 'Italian Republic', 'ITA', '380', 'yes', '39', '.jm', NULL),
(384, 'CI', 'Cote d\'ivoire (Ivory Coast)', 'Republic of C&ocirc;te D\'Ivoire (Ivory Coast)', 'CIV', '384', 'yes', '225', '.ci', NULL),
(388, 'JM', 'Jamaica', 'Jamaica', 'JAM', '388', 'yes', '1+876', '.jm', NULL),
(392, 'JP', 'Japan', 'Japan', 'JPN', '392', 'yes', '81', '.jp', NULL),
(398, 'KZ', 'Kazakhstan', 'Republic of Kazakhstan', 'KAZ', '398', 'yes', '7', '.kz', NULL),
(400, 'JO', 'Jordan', 'Hashemite Kingdom of Jordan', 'JOR', '400', 'yes', '962', '.jo', NULL),
(404, 'KE', 'Kenya', 'Republic of Kenya', 'KEN', '404', 'yes', '254', '.ke', NULL),
(408, 'KP', 'North Korea', 'Democratic People\'s Republic of Korea', 'PRK', '408', 'yes', '850', '.kp', NULL),
(410, 'KR', 'South Korea', 'Republic of Korea', 'KOR', '410', 'yes', '82', '.kr', NULL),
(414, 'KW', 'Kuwait', 'State of Kuwait', 'KWT', '414', 'yes', '965', '.kw', NULL),
(417, 'KG', 'Kyrgyzstan', 'Kyrgyz Republic', 'KGZ', '417', 'yes', '996', '.kg', NULL),
(418, 'LA', 'Laos', 'Lao People\'s Democratic Republic', 'LAO', '418', 'yes', '856', '.la', NULL),
(422, 'LB', 'Lebanon', 'Republic of Lebanon', 'LBN', '422', 'yes', '961', '.lb', NULL),
(426, 'LS', 'Lesotho', 'Kingdom of Lesotho', 'LSO', '426', 'yes', '266', '.ls', NULL),
(428, 'LV', 'Latvia', 'Republic of Latvia', 'LVA', '428', 'yes', '371', '.lv', NULL),
(430, 'LR', 'Liberia', 'Republic of Liberia', 'LBR', '430', 'yes', '231', '.lr', NULL),
(434, 'LY', 'Libya', 'Libya', 'LBY', '434', 'yes', '218', '.ly', NULL),
(438, 'LI', 'Liechtenstein', 'Principality of Liechtenstein', 'LIE', '438', 'yes', '423', '.li', NULL),
(440, 'LT', 'Lithuania', 'Republic of Lithuania', 'LTU', '440', 'yes', '370', '.lt', NULL),
(442, 'LU', 'Luxembourg', 'Grand Duchy of Luxembourg', 'LUX', '442', 'yes', '352', '.lu', NULL),
(446, 'MO', 'Macao', 'The Macao Special Administrative Region', 'MAC', '446', 'no', '853', '.mo', NULL),
(450, 'MG', 'Madagascar', 'Republic of Madagascar', 'MDG', '450', 'yes', '261', '.mg', NULL),
(454, 'MW', 'Malawi', 'Republic of Malawi', 'MWI', '454', 'yes', '265', '.mw', NULL),
(458, 'MY', 'Malaysia', 'Malaysia', 'MYS', '458', 'yes', '60', '.my', NULL),
(462, 'MV', 'Maldives', 'Republic of Maldives', 'MDV', '462', 'yes', '960', '.mv', NULL),
(466, 'ML', 'Mali', 'Republic of Mali', 'MLI', '466', 'yes', '223', '.ml', NULL),
(470, 'MT', 'Malta', 'Republic of Malta', 'MLT', '470', 'yes', '356', '.mt', NULL),
(474, 'MQ', 'Martinique', 'Martinique', 'MTQ', '474', 'no', '596', '.mq', NULL),
(478, 'MR', 'Mauritania', 'Islamic Republic of Mauritania', 'MRT', '478', 'yes', '222', '.mr', NULL),
(480, 'MU', 'Mauritius', 'Republic of Mauritius', 'MUS', '480', 'yes', '230', '.mu', NULL),
(484, 'MX', 'Mexico', 'United Mexican States', 'MEX', '484', 'yes', '52', '.mx', NULL),
(492, 'MC', 'Monaco', 'Principality of Monaco', 'MCO', '492', 'yes', '377', '.mc', NULL),
(496, 'MN', 'Mongolia', 'Mongolia', 'MNG', '496', 'yes', '976', '.mn', NULL),
(498, 'MD', 'Moldava', 'Republic of Moldova', 'MDA', '498', 'yes', '373', '.md', NULL),
(499, 'ME', 'Montenegro', 'Montenegro', 'MNE', '499', 'yes', '382', '.me', NULL),
(500, 'MS', 'Montserrat', 'Montserrat', 'MSR', '500', 'no', '1+664', '.ms', NULL),
(504, 'MA', 'Morocco', 'Kingdom of Morocco', 'MAR', '504', 'yes', '212', '.ma', NULL),
(508, 'MZ', 'Mozambique', 'Republic of Mozambique', 'MOZ', '508', 'yes', '258', '.mz', NULL),
(512, 'OM', 'Oman', 'Sultanate of Oman', 'OMN', '512', 'yes', '968', '.om', NULL),
(516, 'NA', 'Namibia', 'Republic of Namibia', 'NAM', '516', 'yes', '264', '.na', NULL),
(520, 'NR', 'Nauru', 'Republic of Nauru', 'NRU', '520', 'yes', '674', '.nr', NULL),
(524, 'NP', 'Nepal', 'Federal Democratic Republic of Nepal', 'NPL', '524', 'yes', '977', '.np', NULL),
(528, 'NL', 'Netherlands', 'Kingdom of the Netherlands', 'NLD', '528', 'yes', '31', '.nl', NULL),
(531, 'CW', 'Curacao', 'Cura&ccedil;ao', 'CUW', '531', 'no', '599', '.cw', NULL),
(533, 'AW', 'Aruba', 'Aruba', 'ABW', '533', 'no', '297', '.aw', NULL),
(534, 'SX', 'Sint Maarten', 'Sint Maarten', 'SXM', '534', 'no', '1+721', '.sx', NULL),
(535, 'BQ', 'Bonaire, Sint Eustatius and Saba', 'Bonaire, Sint Eustatius and Saba', 'BES', '535', 'no', '599', '.bq', NULL),
(540, 'NC', 'New Caledonia', 'New Caledonia', 'NCL', '540', 'no', '687', '.nc', NULL),
(548, 'VU', 'Vanuatu', 'Republic of Vanuatu', 'VUT', '548', 'yes', '678', '.vu', NULL),
(554, 'NZ', 'New Zealand', 'New Zealand', 'NZL', '554', 'yes', '64', '.nz', NULL),
(558, 'NI', 'Nicaragua', 'Republic of Nicaragua', 'NIC', '558', 'yes', '505', '.ni', NULL),
(562, 'NE', 'Niger', 'Republic of Niger', 'NER', '562', 'yes', '227', '.ne', NULL),
(566, 'NG', 'Nigeria', 'Federal Republic of Nigeria', 'NGA', '566', 'yes', '234', '.ng', NULL),
(570, 'NU', 'Niue', 'Niue', 'NIU', '570', 'some', '683', '.nu', NULL),
(574, 'NF', 'Norfolk Island', 'Norfolk Island', 'NFK', '574', 'no', '672', '.nf', NULL),
(578, 'NO', 'Norway', 'Kingdom of Norway', 'NOR', '578', 'yes', '47', '.no', NULL),
(580, 'MP', 'Northern Mariana Islands', 'Northern Mariana Islands', 'MNP', '580', 'no', '1+670', '.mp', NULL),
(581, 'UM', 'United States Minor Outlying Islands', 'United States Minor Outlying Islands', 'UMI', '581', 'no', 'NONE', 'NONE', NULL),
(583, 'FM', 'Micronesia', 'Federated States of Micronesia', 'FSM', '583', 'yes', '691', '.fm', NULL),
(584, 'MH', 'Marshall Islands', 'Republic of the Marshall Islands', 'MHL', '584', 'yes', '692', '.mh', NULL),
(585, 'PW', 'Palau', 'Republic of Palau', 'PLW', '585', 'yes', '680', '.pw', NULL),
(586, 'PK', 'Pakistan', 'Islamic Republic of Pakistan', 'PAK', '586', 'yes', '92', '.pk', NULL),
(591, 'PA', 'Panama', 'Republic of Panama', 'PAN', '591', 'yes', '507', '.pa', NULL),
(598, 'PG', 'Papua New Guinea', 'Independent State of Papua New Guinea', 'PNG', '598', 'yes', '675', '.pg', NULL),
(600, 'PY', 'Paraguay', 'Republic of Paraguay', 'PRY', '600', 'yes', '595', '.py', NULL),
(604, 'PE', 'Peru', 'Republic of Peru', 'PER', '604', 'yes', '51', '.pe', NULL),
(608, 'PH', 'Phillipines', 'Republic of the Philippines', 'PHL', '608', 'yes', '63', '.ph', NULL),
(612, 'PN', 'Pitcairn', 'Pitcairn', 'PCN', '612', 'no', 'NONE', '.pn', NULL),
(616, 'PL', 'Poland', 'Republic of Poland', 'POL', '616', 'yes', '48', '.pl', NULL),
(620, 'PT', 'Portugal', 'Portuguese Republic', 'PRT', '620', 'yes', '351', '.pt', NULL),
(624, 'GW', 'Guinea-Bissau', 'Republic of Guinea-Bissau', 'GNB', '624', 'yes', '245', '.gw', NULL),
(626, 'TL', 'Timor-Leste (East Timor)', 'Democratic Republic of Timor-Leste', 'TLS', '626', 'yes', '670', '.tl', NULL),
(630, 'PR', 'Puerto Rico', 'Commonwealth of Puerto Rico', 'PRI', '630', 'no', '1+939', '.pr', NULL),
(634, 'QA', 'Qatar', 'State of Qatar', 'QAT', '634', 'yes', '974', '.qa', NULL),
(638, 'RE', 'Reunion', 'R&eacute;union', 'REU', '638', 'no', '262', '.re', NULL),
(642, 'RO', 'Romania', 'Romania', 'ROU', '642', 'yes', '40', '.ro', NULL),
(643, 'RU', 'Russia', 'Russian Federation', 'RUS', '643', 'yes', '7', '.ru', NULL),
(646, 'RW', 'Rwanda', 'Republic of Rwanda', 'RWA', '646', 'yes', '250', '.rw', NULL),
(652, 'BL', 'Saint Barthelemy', 'Saint Barth&eacute;lemy', 'BLM', '652', 'no', '590', '.bl', NULL),
(654, 'SH', 'Saint Helena', 'Saint Helena, Ascension and Tristan da Cunha', 'SHN', '654', 'no', '290', '.sh', NULL),
(659, 'KN', 'Saint Kitts and Nevis', 'Federation of Saint Christopher and Nevis', 'KNA', '659', 'yes', '1+869', '.kn', NULL),
(660, 'AI', 'Anguilla', 'Anguilla', 'AIA', '660', 'no', '1+264', '.ai', NULL),
(662, 'LC', 'Saint Lucia', 'Saint Lucia', 'LCA', '662', 'yes', '1+758', '.lc', NULL),
(663, 'MF', 'Saint Martin', 'Saint Martin', 'MAF', '663', 'no', '590', '.mf', NULL),
(666, 'PM', 'Saint Pierre and Miquelon', 'Saint Pierre and Miquelon', 'SPM', '666', 'no', '508', '.pm', NULL),
(670, 'VC', 'Saint Vincent and the Grenadines', 'Saint Vincent and the Grenadines', 'VCT', '670', 'yes', '1+784', '.vc', NULL),
(674, 'SM', 'San Marino', 'Republic of San Marino', 'SMR', '674', 'yes', '378', '.sm', NULL),
(678, 'ST', 'Sao Tome and Principe', 'Democratic Republic of S&atilde;o Tom&eacute; and Pr&iacute;ncipe', 'STP', '678', 'yes', '239', '.st', NULL),
(682, 'SA', 'Saudi Arabia', 'Kingdom of Saudi Arabia', 'SAU', '682', 'yes', '966', '.sa', NULL),
(686, 'SN', 'Senegal', 'Republic of Senegal', 'SEN', '686', 'yes', '221', '.sn', NULL),
(688, 'RS', 'Serbia', 'Republic of Serbia', 'SRB', '688', 'yes', '381', '.rs', NULL),
(690, 'SC', 'Seychelles', 'Republic of Seychelles', 'SYC', '690', 'yes', '248', '.sc', NULL),
(694, 'SL', 'Sierra Leone', 'Republic of Sierra Leone', 'SLE', '694', 'yes', '232', '.sl', NULL),
(702, 'SG', 'Singapore', 'Republic of Singapore', 'SGP', '702', 'yes', '65', '.sg', NULL),
(703, 'SK', 'Slovakia', 'Slovak Republic', 'SVK', '703', 'yes', '421', '.sk', NULL),
(704, 'VN', 'Vietnam', 'Socialist Republic of Vietnam', 'VNM', '704', 'yes', '84', '.vn', NULL),
(705, 'SI', 'Slovenia', 'Republic of Slovenia', 'SVN', '705', 'yes', '386', '.si', NULL),
(706, 'SO', 'Somalia', 'Somali Republic', 'SOM', '706', 'yes', '252', '.so', NULL),
(710, 'ZA', 'South Africa', 'Republic of South Africa', 'ZAF', '710', 'yes', '27', '.za', NULL),
(716, 'ZW', 'Zimbabwe', 'Republic of Zimbabwe', 'ZWE', '716', 'yes', '263', '.zw', NULL),
(724, 'ES', 'Spain', 'Kingdom of Spain', 'ESP', '724', 'yes', '34', '.es', NULL),
(728, 'SS', 'South Sudan', 'Republic of South Sudan', 'SSD', '728', 'yes', '211', '.ss', NULL),
(729, 'SD', 'Sudan', 'Republic of the Sudan', 'SDN', '729', 'yes', '249', '.sd', NULL),
(732, 'EH', 'Western Sahara', 'Western Sahara', 'ESH', '732', 'no', '212', '.eh', NULL),
(740, 'SR', 'Suriname', 'Republic of Suriname', 'SUR', '740', 'yes', '597', '.sr', NULL),
(744, 'SJ', 'Svalbard and Jan Mayen', 'Svalbard and Jan Mayen', 'SJM', '744', 'no', '47', '.sj', NULL),
(748, 'SZ', 'Swaziland', 'Kingdom of Swaziland', 'SWZ', '748', 'yes', '268', '.sz', NULL),
(752, 'SE', 'Sweden', 'Kingdom of Sweden', 'SWE', '752', 'yes', '46', '.se', NULL),
(756, 'CH', 'Switzerland', 'Swiss Confederation', 'CHE', '756', 'yes', '41', '.ch', NULL),
(760, 'SY', 'Syria', 'Syrian Arab Republic', 'SYR', '760', 'yes', '963', '.sy', NULL),
(762, 'TJ', 'Tajikistan', 'Republic of Tajikistan', 'TJK', '762', 'yes', '992', '.tj', NULL),
(764, 'TH', 'Thailand', 'Kingdom of Thailand', 'THA', '764', 'yes', '66', '.th', NULL),
(768, 'TG', 'Togo', 'Togolese Republic', 'TGO', '768', 'yes', '228', '.tg', NULL),
(772, 'TK', 'Tokelau', 'Tokelau', 'TKL', '772', 'no', '690', '.tk', NULL),
(776, 'TO', 'Tonga', 'Kingdom of Tonga', 'TON', '776', 'yes', '676', '.to', NULL),
(780, 'TT', 'Trinidad and Tobago', 'Republic of Trinidad and Tobago', 'TTO', '780', 'yes', '1+868', '.tt', NULL),
(784, 'AE', 'United Arab Emirates', 'United Arab Emirates', 'ARE', '784', 'yes', '971', '.ae', NULL),
(788, 'TN', 'Tunisia', 'Republic of Tunisia', 'TUN', '788', 'yes', '216', '.tn', NULL),
(792, 'TR', 'Turkey', 'Republic of Turkey', 'TUR', '792', 'yes', '90', '.tr', NULL),
(795, 'TM', 'Turkmenistan', 'Turkmenistan', 'TKM', '795', 'yes', '993', '.tm', NULL),
(796, 'TC', 'Turks and Caicos Islands', 'Turks and Caicos Islands', 'TCA', '796', 'no', '1+649', '.tc', NULL),
(798, 'TV', 'Tuvalu', 'Tuvalu', 'TUV', '798', 'yes', '688', '.tv', NULL),
(800, 'UG', 'Uganda', 'Republic of Uganda', 'UGA', '800', 'yes', '256', '.ug', NULL),
(804, 'UA', 'Ukraine', 'Ukraine', 'UKR', '804', 'yes', '380', '.ua', NULL),
(807, 'MK', 'Macedonia', 'The Former Yugoslav Republic of Macedonia', 'MKD', '807', 'yes', '389', '.mk', NULL),
(818, 'EG', 'Egypt', 'Arab Republic of Egypt', 'EGY', '818', 'yes', '20', '.eg', NULL),
(826, 'GB', 'United Kingdom', 'United Kingdom of Great Britain and Nothern Ireland', 'GBR', '826', 'yes', '44', '.uk', NULL),
(831, 'GG', 'Guernsey', 'Guernsey', 'GGY', '831', 'no', '44', '.gg', NULL),
(832, 'JE', 'Jersey', 'The Bailiwick of Jersey', 'JEY', '832', 'no', '44', '.je', NULL),
(833, 'IM', 'Isle of Man', 'Isle of Man', 'IMN', '833', 'no', '44', '.im', NULL),
(834, 'TZ', 'Tanzania', 'United Republic of Tanzania', 'TZA', '834', 'yes', '255', '.tz', NULL),
(840, 'US', 'United States', 'United States of America', 'USA', '840', 'yes', '1', '.us', NULL),
(850, 'VI', 'Virgin Islands, US', 'Virgin Islands of the United States', 'VIR', '850', 'no', '1+340', '.vi', NULL),
(854, 'BF', 'Burkina Faso', 'Burkina Faso', 'BFA', '854', 'yes', '226', '.bf', NULL),
(858, 'UY', 'Uruguay', 'Eastern Republic of Uruguay', 'URY', '858', 'yes', '598', '.uy', NULL),
(860, 'UZ', 'Uzbekistan', 'Republic of Uzbekistan', 'UZB', '860', 'yes', '998', '.uz', NULL),
(862, 'VE', 'Venezuela', 'Bolivarian Republic of Venezuela', 'VEN', '862', 'yes', '58', '.ve', NULL),
(876, 'WF', 'Wallis and Futuna', 'Wallis and Futuna', 'WLF', '876', 'no', '681', '.wf', NULL),
(882, 'WS', 'Samoa', 'Independent State of Samoa', 'WSM', '882', 'yes', '685', '.ws', NULL),
(887, 'YE', 'Yemen', 'Republic of Yemen', 'YEM', '887', 'yes', '967', '.ye', NULL),
(894, 'ZM', 'Zambia', 'Republic of Zambia', 'ZMB', '894', 'yes', '260', '.zm', NULL);

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `databasechangelog`
--

CREATE TABLE IF NOT EXISTS `databasechangelog` (
  `ID` varchar(255) NOT NULL,
  `AUTHOR` varchar(255) NOT NULL,
  `FILENAME` varchar(255) NOT NULL,
  `DATEEXECUTED` datetime NOT NULL,
  `ORDEREXECUTED` int(11) NOT NULL,
  `EXECTYPE` varchar(10) NOT NULL,
  `MD5SUM` varchar(35) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `COMMENTS` varchar(255) DEFAULT NULL,
  `TAG` varchar(255) DEFAULT NULL,
  `LIQUIBASE` varchar(20) DEFAULT NULL,
  `CONTEXTS` varchar(255) DEFAULT NULL,
  `LABELS` varchar(255) DEFAULT NULL,
  `DEPLOYMENT_ID` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Andmete tõmmistamine tabelile `databasechangelog`
--

INSERT IGNORE INTO `databasechangelog` (`ID`, `AUTHOR`, `FILENAME`, `DATEEXECUTED`, `ORDEREXECUTED`, `EXECTYPE`, `MD5SUM`, `DESCRIPTION`, `COMMENTS`, `TAG`, `LIQUIBASE`, `CONTEXTS`, `LABELS`, `DEPLOYMENT_ID`) VALUES
('00000000000002', 'jhipster', 'config/liquibase/changelog/00000000000000_initial_schema.xml', '2023-03-24 18:34:39', 1, 'EXECUTED', '8:fc8e5b393c90215202ec38612ed14f68', 'createTable tableName=jhi_date_time_wrapper', '', NULL, '4.3.1', 'test', NULL, '6603679183'),
('20210324154857-1', 'jhipster', 'config/liquibase/changelog/20210324154857_added_entity_Region.xml', '2023-03-24 18:34:39', 2, 'EXECUTED', '8:9ab06524acf69b3ca8131601274c80e8', 'createTable tableName=region', '', NULL, '4.3.1', NULL, NULL, '6603679183'),
('20210324154857-1-data', 'jhipster', 'config/liquibase/changelog/20210324154857_added_entity_Region.xml', '2023-03-24 18:34:39', 3, 'EXECUTED', '8:ae8bb4c797465495a4567812e5a8f985', 'loadData tableName=region', '', NULL, '4.3.1', 'faker', NULL, '6603679183'),
('20210324154858-1', 'jhipster', 'config/liquibase/changelog/20210324154858_added_entity_Country.xml', '2023-03-24 18:34:39', 4, 'EXECUTED', '8:488b17599e2a9cfaed10f3b7682294f1', 'createTable tableName=country', '', NULL, '4.3.1', NULL, NULL, '6603679183'),
('20210324154858-1-data', 'jhipster', 'config/liquibase/changelog/20210324154858_added_entity_Country.xml', '2023-03-24 18:34:39', 5, 'EXECUTED', '8:b941a4f6607daaf87bb59fe1e2d58223', 'loadData tableName=country', '', NULL, '4.3.1', 'faker', NULL, '6603679183'),
('20210324154859-1', 'jhipster', 'config/liquibase/changelog/20210324154859_added_entity_Settlement.xml', '2023-03-24 18:34:39', 6, 'EXECUTED', '8:f3f998d9b7176fc1b4e8d4100cb84136', 'createTable tableName=settlement', '', NULL, '4.3.1', NULL, NULL, '6603679183'),
('20210324154859-1-data', 'jhipster', 'config/liquibase/changelog/20210324154859_added_entity_Settlement.xml', '2023-03-24 18:34:39', 7, 'EXECUTED', '8:0dd21f62191cbeb8bd3bde16833ec3bc', 'loadData tableName=settlement', '', NULL, '4.3.1', 'faker', NULL, '6603679183'),
('20210324154900-1', 'jhipster', 'config/liquibase/changelog/20210324154900_added_entity_TerritorialUnit.xml', '2023-03-24 18:34:39', 8, 'EXECUTED', '8:3054f9bb0356758bafe17ca3ec7de02c', 'createTable tableName=territorial_unit', '', NULL, '4.3.1', NULL, NULL, '6603679183'),
('20210324154900-1-data', 'jhipster', 'config/liquibase/changelog/20210324154900_added_entity_TerritorialUnit.xml', '2023-03-24 18:34:39', 9, 'EXECUTED', '8:f335c9d091fccf2e34813dd2d13f64b3', 'loadData tableName=territorial_unit', '', NULL, '4.3.1', 'faker', NULL, '6603679183'),
('20210324154901-1', 'jhipster', 'config/liquibase/changelog/20210324154901_added_entity_Address.xml', '2023-03-24 18:37:46', 10, 'EXECUTED', '8:870432b2fc542f050aecf11f388937fd', 'createTable tableName=address', '', NULL, '4.3.1', NULL, NULL, '6603866119'),
('20210324154901-1-data', 'jhipster', 'config/liquibase/changelog/20210324154901_added_entity_Address.xml', '2023-03-24 18:37:46', 11, 'EXECUTED', '8:80013d2812394c87e8a9b510ddc000cc', 'loadData tableName=address', '', NULL, '4.3.1', 'faker', NULL, '6603866119'),
('20210324154902-1', 'jhipster', 'config/liquibase/changelog/20210324154902_added_entity_PrimaryPhone.xml', '2023-03-24 18:37:46', 12, 'EXECUTED', '8:7141a0ce02a1ec2fd01d17a3d1f84bd3', 'createTable tableName=primary_phone', '', NULL, '4.3.1', NULL, NULL, '6603866119'),
('20210324154902-1-data', 'jhipster', 'config/liquibase/changelog/20210324154902_added_entity_PrimaryPhone.xml', '2023-03-24 18:37:46', 13, 'EXECUTED', '8:1aa42f118a5fe75e5d0751f9897b8ddb', 'loadData tableName=primary_phone', '', NULL, '4.3.1', 'faker', NULL, '6603866119'),
('20210324154903-1', 'jhipster', 'config/liquibase/changelog/20210324154903_added_entity_PrimaryEmail.xml', '2023-03-24 18:37:46', 14, 'EXECUTED', '8:e3a00b30246609a0a5c60e1072bb5fca', 'createTable tableName=primary_email', '', NULL, '4.3.1', NULL, NULL, '6603866119'),
('20210324154903-1-data', 'jhipster', 'config/liquibase/changelog/20210324154903_added_entity_PrimaryEmail.xml', '2023-03-24 18:37:46', 15, 'EXECUTED', '8:e1d3d879b4f1f0cf8ad5c894a2cb9161', 'loadData tableName=primary_email', '', NULL, '4.3.1', 'faker', NULL, '6603866119'),
('20210324154904-1', 'jhipster', 'config/liquibase/changelog/20210324154904_added_entity_PrimaryContactInfo.xml', '2023-03-24 18:37:46', 16, 'EXECUTED', '8:cc02d3e988292303f9e15ed3a5083c11', 'createTable tableName=primary_contact_info', '', NULL, '4.3.1', NULL, NULL, '6603866119'),
('20210324154904-1-data', 'jhipster', 'config/liquibase/changelog/20210324154904_added_entity_PrimaryContactInfo.xml', '2023-03-24 18:37:46', 17, 'EXECUTED', '8:27ea69a2337043844d3abbf8b551a180', 'loadData tableName=primary_contact_info', '', NULL, '4.3.1', 'faker', NULL, '6603866119'),
('20210324154905-1', 'jhipster', 'config/liquibase/changelog/20210324154905_added_entity_Person.xml', '2023-03-24 18:37:46', 18, 'EXECUTED', '8:06e43f6ae382a4254dcf0081be8a4c65', 'createTable tableName=person', '', NULL, '4.3.1', NULL, NULL, '6603866119'),
('20210324154905-1-data', 'jhipster', 'config/liquibase/changelog/20210324154905_added_entity_Person.xml', '2023-03-24 18:37:46', 19, 'EXECUTED', '8:79aef70b7a76648379993db318785263', 'loadData tableName=person', '', NULL, '4.3.1', 'faker', NULL, '6603866119'),
('20210324154906-1', 'jhipster', 'config/liquibase/changelog/20210324154906_added_entity_Organisation.xml', '2023-03-24 18:37:46', 20, 'EXECUTED', '8:001d3dbbe62c4ba92f66bf2ed7a509b1', 'createTable tableName=organisation', '', NULL, '4.3.1', NULL, NULL, '6603866119'),
('20210324154906-1-data', 'jhipster', 'config/liquibase/changelog/20210324154906_added_entity_Organisation.xml', '2023-03-24 18:37:46', 21, 'EXECUTED', '8:3b693ada9d0f95ac7ca6476383a5f082', 'loadData tableName=organisation', '', NULL, '4.3.1', 'faker', NULL, '6603866119'),
('20210324154907-1', 'jhipster', 'config/liquibase/changelog/20210324154907_added_entity_FactBook.xml', '2023-03-24 18:37:46', 22, 'EXECUTED', '8:11772d536feba4fadbd619a3db99b324', 'createTable tableName=fact_book', '', NULL, '4.3.1', NULL, NULL, '6603866119'),
('20210324154907-1-data', 'jhipster', 'config/liquibase/changelog/20210324154907_added_entity_FactBook.xml', '2023-03-24 18:37:46', 23, 'EXECUTED', '8:b7045a0572a8187cd7f41d97905ab3b5', 'loadData tableName=fact_book', '', NULL, '4.3.1', 'faker', NULL, '6603866119'),
('20210324154908-1', 'jhipster', 'config/liquibase/changelog/20210324154908_added_entity_FactBookInfo.xml', '2023-03-24 18:37:46', 24, 'EXECUTED', '8:890a3a65aac183768ab6eb00fddc09c0', 'createTable tableName=fact_book_info', '', NULL, '4.3.1', NULL, NULL, '6603866119'),
('20210324154908-1-data', 'jhipster', 'config/liquibase/changelog/20210324154908_added_entity_FactBookInfo.xml', '2023-03-24 18:37:46', 25, 'EXECUTED', '8:5b468888b66c468aea94f8e7b888b1e3', 'loadData tableName=fact_book_info', '', NULL, '4.3.1', 'faker', NULL, '6603866119'),
('20210324154909-1', 'jhipster', 'config/liquibase/changelog/20210324154909_added_entity_FactBookItem.xml', '2023-03-24 18:37:46', 26, 'EXECUTED', '8:abb850d96d152175a7fbd91f165c2ee9', 'createTable tableName=fact_book_item', '', NULL, '4.3.1', NULL, NULL, '6603866119'),
('20210324154909-1-relations', 'jhipster', 'config/liquibase/changelog/20210324154909_added_entity_FactBookItem.xml', '2023-03-24 18:37:46', 27, 'EXECUTED', '8:849ef46abbfb9a2d0629794ac0768320', 'createTable tableName=rel_fact_book_item__related; addPrimaryKey tableName=rel_fact_book_item__related', '', NULL, '4.3.1', NULL, NULL, '6603866119'),
('20210324154909-1-data', 'jhipster', 'config/liquibase/changelog/20210324154909_added_entity_FactBookItem.xml', '2023-03-24 18:37:46', 28, 'EXECUTED', '8:1b94f23cb854185554d9487581f423fd', 'loadData tableName=fact_book_item', '', NULL, '4.3.1', 'faker', NULL, '6603866119'),
('20210324154910-1', 'jhipster', 'config/liquibase/changelog/20210324154910_added_entity_FactBookItemInfo.xml', '2023-03-24 18:37:46', 29, 'EXECUTED', '8:d9d336c815a1d101dae85ff9c38d2d2e', 'createTable tableName=fact_book_item_info', '', NULL, '4.3.1', NULL, NULL, '6603866119'),
('20210324154910-1-data', 'jhipster', 'config/liquibase/changelog/20210324154910_added_entity_FactBookItemInfo.xml', '2023-03-24 18:37:46', 30, 'EXECUTED', '8:03e0b90ce9ae8bce311637162b544361', 'loadData tableName=fact_book_item_info', '', NULL, '4.3.1', 'faker', NULL, '6603866119'),
('20210324154911-1', 'jhipster', 'config/liquibase/changelog/20210324154911_added_entity_About.xml', '2023-03-24 18:37:46', 31, 'EXECUTED', '8:d9609c6691107c060288eb7be8be3bef', 'createTable tableName=about', '', NULL, '4.3.1', NULL, NULL, '6603866119'),
('20210324154911-1-data', 'jhipster', 'config/liquibase/changelog/20210324154911_added_entity_About.xml', '2023-03-24 18:37:47', 32, 'EXECUTED', '8:d964e759774612f2e25d9975c8af0b85', 'loadData tableName=about', '', NULL, '4.3.1', 'faker', NULL, '6603866119'),
('20210324154912-1', 'jhipster', 'config/liquibase/changelog/20210324154912_added_entity_Event.xml', '2023-03-24 18:37:47', 33, 'EXECUTED', '8:8fb74fcf9d03d96c62b5efd778b2ea42', 'createTable tableName=event', '', NULL, '4.3.1', NULL, NULL, '6603866119'),
('20210324154912-1-data', 'jhipster', 'config/liquibase/changelog/20210324154912_added_entity_Event.xml', '2023-03-24 18:37:47', 34, 'EXECUTED', '8:edc4785afbab87ae0522b8c7eb23659b', 'loadData tableName=event', '', NULL, '4.3.1', 'faker', NULL, '6603866119'),
('20210324154913-1', 'jhipster', 'config/liquibase/changelog/20210324154913_added_entity_Comment.xml', '2023-03-24 18:37:47', 35, 'EXECUTED', '8:1565e9b63529d93a039caf125e53a78b', 'createTable tableName=comment', '', NULL, '4.3.1', NULL, NULL, '6603866119'),
('20210324154913-1-data', 'jhipster', 'config/liquibase/changelog/20210324154913_added_entity_Comment.xml', '2023-03-24 18:37:47', 36, 'EXECUTED', '8:44a595202922a3cf99eab946b774076c', 'loadData tableName=comment', '', NULL, '4.3.1', 'faker', NULL, '6603866119'),
('20210324154914-1', 'jhipster', 'config/liquibase/changelog/20210324154914_added_entity_EventRegistration.xml', '2023-03-24 18:37:47', 37, 'EXECUTED', '8:3ec63b42de96ffc16d098754554f5d41', 'createTable tableName=event_registration', '', NULL, '4.3.1', NULL, NULL, '6603866119'),
('20210324154914-1-data', 'jhipster', 'config/liquibase/changelog/20210324154914_added_entity_EventRegistration.xml', '2023-03-24 18:37:47', 38, 'EXECUTED', '8:f73a64451f40eea9cc9db05c40e19b96', 'loadData tableName=event_registration', '', NULL, '4.3.1', 'faker', NULL, '6603866119'),
('20210324154915-1', 'jhipster', 'config/liquibase/changelog/20210324154915_added_entity_EventOrganizerAndContact.xml', '2023-03-24 18:37:47', 39, 'EXECUTED', '8:ff3fa23f623a797b421becb7415a7087', 'createTable tableName=event_organizer_and_contact', '', NULL, '4.3.1', NULL, NULL, '6603866119'),
('20210324154915-1-data', 'jhipster', 'config/liquibase/changelog/20210324154915_added_entity_EventOrganizerAndContact.xml', '2023-03-24 18:37:47', 40, 'EXECUTED', '8:2882e34952567318e16a094f741022c7', 'loadData tableName=event_organizer_and_contact', '', NULL, '4.3.1', 'faker', NULL, '6603866119'),
('20210324154916-1', 'jhipster', 'config/liquibase/changelog/20210324154916_added_entity_EventOrganizerStaff.xml', '2023-03-24 18:37:47', 41, 'EXECUTED', '8:9dfdc9d90efc6a8017d60e941309d316', 'createTable tableName=event_organizer_staff', '', NULL, '4.3.1', NULL, NULL, '6603866119'),
('20210324154916-1-data', 'jhipster', 'config/liquibase/changelog/20210324154916_added_entity_EventOrganizerStaff.xml', '2023-03-24 18:37:47', 42, 'EXECUTED', '8:3f303167b02b2dec536f138075c60157', 'loadData tableName=event_organizer_staff', '', NULL, '4.3.1', 'faker', NULL, '6603866119'),
('20210324154917-1', 'jhipster', 'config/liquibase/changelog/20210324154917_added_entity_EventTimeAndVenue.xml', '2023-03-24 18:37:47', 43, 'EXECUTED', '8:88e0cc804b1907e4b3eb15b30139e219', 'createTable tableName=event_time_and_venue', '', NULL, '4.3.1', NULL, NULL, '6603866119'),
('20210324154917-1-data', 'jhipster', 'config/liquibase/changelog/20210324154917_added_entity_EventTimeAndVenue.xml', '2023-03-24 18:37:47', 44, 'EXECUTED', '8:02b57e97c9e9c74bf90fe18489e0ea88', 'loadData tableName=event_time_and_venue', '', NULL, '4.3.1', 'faker', NULL, '6603866119'),
('20210324154918-1', 'jhipster', 'config/liquibase/changelog/20210324154918_added_entity_Image.xml', '2023-03-24 18:37:47', 45, 'EXECUTED', '8:41be80efb9a8c3cc3f481aa6bbc103ef', 'createTable tableName=image', '', NULL, '4.3.1', NULL, NULL, '6603866119'),
('20210324154918-1-data', 'jhipster', 'config/liquibase/changelog/20210324154918_added_entity_Image.xml', '2023-03-24 18:37:47', 46, 'EXECUTED', '8:455fe2a4deca8d446fe989e39981a9b9', 'loadData tableName=image', '', NULL, '4.3.1', 'faker', NULL, '6603866119'),
('20210324154919-1', 'jhipster', 'config/liquibase/changelog/20210324154919_added_entity_Note.xml', '2023-03-24 18:37:47', 47, 'EXECUTED', '8:2ad17e4b4671eaf1235f5eacdc5cb1b2', 'createTable tableName=note', '', NULL, '4.3.1', NULL, NULL, '6603866119'),
('20210324154919-1-data', 'jhipster', 'config/liquibase/changelog/20210324154919_added_entity_Note.xml', '2023-03-24 18:37:47', 48, 'EXECUTED', '8:cd1cf9bf72270c9f058dc3aa0b4bef36', 'loadData tableName=note', '', NULL, '4.3.1', 'faker', NULL, '6603866119'),
('20210324154920-1', 'jhipster', 'config/liquibase/changelog/20210324154920_added_entity_UserPerson.xml', '2023-03-24 18:37:47', 49, 'EXECUTED', '8:5671ccc62ff2deddc115059cc1fe4774', 'createTable tableName=user_person', '', NULL, '4.3.1', NULL, NULL, '6603866119'),
('20210324154920-1-data', 'jhipster', 'config/liquibase/changelog/20210324154920_added_entity_UserPerson.xml', '2023-03-24 18:37:47', 50, 'EXECUTED', '8:05b0637a8ba603dab516cf4061e6e59b', 'loadData tableName=user_person', '', NULL, '4.3.1', 'faker', NULL, '6603866119'),
('20210324154921-1', 'jhipster', 'config/liquibase/changelog/20210324154921_added_entity_Venue.xml', '2023-03-24 18:37:47', 51, 'EXECUTED', '8:024b72d96c53b43c3b3537a0d655aafe', 'createTable tableName=venue', '', NULL, '4.3.1', NULL, NULL, '6603866119'),
('20210324154921-1-data', 'jhipster', 'config/liquibase/changelog/20210324154921_added_entity_Venue.xml', '2023-03-24 18:37:47', 52, 'EXECUTED', '8:27cb613b668c04da91e076f48060ea6a', 'loadData tableName=venue', '', NULL, '4.3.1', 'faker', NULL, '6603866119'),
('20210324154858-2', 'jhipster', 'config/liquibase/changelog/20210324154858_added_entity_constraints_Country.xml', '2023-03-24 18:37:47', 53, 'EXECUTED', '8:5daba28608066b2011303785046f063f', 'addForeignKeyConstraint baseTableName=country, constraintName=fk_country__region_id, referencedTableName=region', '', NULL, '4.3.1', NULL, NULL, '6603866119'),
('20210324154859-2', 'jhipster', 'config/liquibase/changelog/20210324154859_added_entity_constraints_Settlement.xml', '2023-03-24 18:37:47', 54, 'EXECUTED', '8:f60e7ff8102e537fd22d34364c661dfd', 'addForeignKeyConstraint baseTableName=settlement, constraintName=fk_settlement__country_id, referencedTableName=country; addForeignKeyConstraint baseTableName=settlement, constraintName=fk_settlement__territorial_unit_id, referencedTableName=terri...', '', NULL, '4.3.1', NULL, NULL, '6603866119'),
('20210324154900-2', 'jhipster', 'config/liquibase/changelog/20210324154900_added_entity_constraints_TerritorialUnit.xml', '2023-03-24 18:37:47', 55, 'EXECUTED', '8:387056abba5b6088c4f5701b446ea746', 'addForeignKeyConstraint baseTableName=territorial_unit, constraintName=fk_territorial_unit__country_id, referencedTableName=country', '', NULL, '4.3.1', NULL, NULL, '6603866119');

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `databasechangeloglock`
--

CREATE TABLE IF NOT EXISTS `databasechangeloglock` (
  `ID` int(11) NOT NULL,
  `LOCKED` bit(1) NOT NULL,
  `LOCKGRANTED` datetime DEFAULT NULL,
  `LOCKEDBY` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Andmete tõmmistamine tabelile `databasechangeloglock`
--

INSERT IGNORE INTO `databasechangeloglock` (`ID`, `LOCKED`, `LOCKGRANTED`, `LOCKEDBY`) VALUES
(1, b'0', NULL, NULL);

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `event`
--

CREATE TABLE IF NOT EXISTS `event` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) DEFAULT NULL,
  `tag` varchar(255) DEFAULT NULL,
  `event_organizer_and_contact_id` bigint(20) DEFAULT NULL,
  `is_part_of_event_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_event__event_organizer_and_contact_id` (`event_organizer_and_contact_id`),
  KEY `FKajg25so5i85r066g4bye45o24` (`is_part_of_event_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;

--
-- Andmete tõmmistamine tabelile `event`
--

INSERT IGNORE INTO `event` (`id`, `type`, `tag`, `event_organizer_and_contact_id`, `is_part_of_event_id`) VALUES
(1, 'REGULAR', 'Response', 1, NULL),
(2, 'REGULAR', 'connect Concrete', 3, NULL),
(6, 'REGULAR', 'newexample', 11, NULL),
(7, 'REGULAR', 'newexample', 12, NULL),
(8, 'REGULAR', 'newexample', 13, NULL),
(9, 'REGULAR', 'newexample', 14, NULL),
(10, 'REGULAR', 'newexample', 15, NULL),
(11, 'REGULAR', 'newexample', 16, NULL);

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `event_organizer_and_contact`
--

CREATE TABLE IF NOT EXISTS `event_organizer_and_contact` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `organisation_id` bigint(20) DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKcyvidakof5ybg3xyv0la3iso8` (`organisation_id`),
  KEY `FK56fuqbi252syalonydbxkojfr` (`created_by`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4;

--
-- Andmete tõmmistamine tabelile `event_organizer_and_contact`
--

INSERT IGNORE INTO `event_organizer_and_contact` (`id`, `organisation_id`, `created_by`) VALUES
(1, 1, 1050),
(2, NULL, 1052),
(3, NULL, NULL),
(4, NULL, NULL),
(5, NULL, NULL),
(6, NULL, NULL),
(7, NULL, NULL),
(8, NULL, NULL),
(9, NULL, NULL),
(10, NULL, NULL),
(11, 1, 1050),
(12, 1, 1050),
(13, 1, 1050),
(14, 1, 1050),
(15, 2, 1050),
(16, 2, 1050);

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `event_organizer_staff`
--

CREATE TABLE IF NOT EXISTS `event_organizer_staff` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `phone` int(11) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  `event_organizer_and_contact_id` bigint(20) DEFAULT NULL,
  `person_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKkjwgcdyj46pvh0brdek2fwg7j` (`event_organizer_and_contact_id`),
  KEY `FKg22s3q0nmj80tewxeu4idltto` (`person_id`),
  KEY `FKhs33jq7at09wc77fqfpyp8uv8` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4;

--
-- Andmete tõmmistamine tabelile `event_organizer_staff`
--

INSERT IGNORE INTO `event_organizer_staff` (`id`, `email`, `phone`, `role`, `event_organizer_and_contact_id`, `person_id`, `user_id`) VALUES
(1, 'jaanus@event.ee', 53411383, 'MANAGER', 1, 1, NULL),
(2, 'zahidul@event.ee', 53287343, 'SPECIALCONTACT', 1, 2, NULL),
(3, 'afsana@event.ee', 54321234, 'CONTACTPERSON', 16, 3, NULL),
(4, 'duracell@gmail.com', 987654321, 'MANAGER', NULL, NULL, NULL),
(5, 'andrus@event.ee', 56187654, 'SPECIALCONTACT', NULL, NULL, NULL),
(6, 'hazzan@event.ee', 34562738, 'CONTACTPERSON', NULL, NULL, NULL),
(7, 'Rico.Ledner29@yahoo.com', 36837, 'SPECIALCONTACT', NULL, NULL, NULL),
(8, 'Jerel25@yahoo.com', 57373, 'SPECIALCONTACT', NULL, NULL, NULL),
(9, 'Spencer_Kuhn89@gmail.com', 73733, 'SPECIALCONTACT', NULL, NULL, NULL),
(10, 'Nicholas_Reilly16@gmail.com', 59362, 'MANAGER', NULL, NULL, NULL),
(11, 'jaanus@event.ee', 53411383, 'MANAGER', 14, 1, NULL),
(12, 'zahidul@event.ee', 53287343, 'SPECIALCONTACT', 14, 2, NULL),
(13, 'jaanus@event.ee', 53411383, 'MANAGER', 15, 1, NULL),
(14, 'zahidul@event.ee', 53287343, 'SPECIALCONTACT', 15, 2, NULL),
(15, 'jaanus@event.ee', 53411383, 'MANAGER', 16, 1, NULL),
(16, 'zahidul@event.ee', 53287343, 'SPECIALCONTACT', 16, 2, NULL);

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `event_registration`
--

CREATE TABLE IF NOT EXISTS `event_registration` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `all_ocurrences` bit(1) DEFAULT NULL,
  `all_sub_events` bit(1) DEFAULT NULL,
  `approved` bit(1) DEFAULT NULL,
  `registering_user` int(11) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `event_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK2ra3ocp6gfw2vbwybbooxxuml` (`event_id`),
  KEY `FK70hjvx9rgobx810q0lqdqs9yq` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

--
-- Andmete tõmmistamine tabelile `event_registration`
--

INSERT IGNORE INTO `event_registration` (`id`, `all_ocurrences`, `all_sub_events`, `approved`, `registering_user`, `user_id`, `event_id`) VALUES
(1, b'0', b'0', b'1', 1050, 2, 1);

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `event_time_venue`
--

CREATE TABLE IF NOT EXISTS `event_time_venue` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `start_date` date DEFAULT NULL,
  `start_time` time DEFAULT NULL,
  `end_time_determined` bit(1) DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `event_id` bigint(20) DEFAULT NULL,
  `venue_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK47gottnr6qgjb9pq2xlri8xsk` (`event_id`),
  KEY `FKea2pdairlokrqhll5f1c9mnfy` (`venue_id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8mb4;

--
-- Andmete tõmmistamine tabelile `event_time_venue`
--

INSERT IGNORE INTO `event_time_venue` (`id`, `start_date`, `start_time`, `end_time_determined`, `end_date`, `end_time`, `event_id`, `venue_id`) VALUES
(1, '2023-01-23', '18:00:00', b'0', NULL, NULL, NULL, NULL),
(2, '2023-04-26', '00:00:00', b'0', NULL, NULL, 1, 2),
(3, '2023-05-01', '12:00:00', b'0', NULL, NULL, 2, 2),
(13, '2023-06-15', '18:00:00', b'0', NULL, NULL, 2, 3),
(14, '2023-05-08', '20:22:00', b'1', NULL, '21:23:00', 1, 2),
(16, '2023-05-06', '13:15:00', b'0', NULL, NULL, 1, 1),
(17, '2023-05-01', NULL, b'0', NULL, NULL, 1, 2),
(19, '2023-04-24', '00:00:00', b'0', NULL, NULL, 7, 2),
(20, '2023-05-08', '20:22:00', b'1', NULL, '21:23:00', 7, 2),
(21, '2023-05-06', '13:15:00', b'0', NULL, NULL, 7, 1),
(22, '2023-05-01', NULL, b'0', NULL, NULL, 7, 2),
(23, '2023-04-24', '00:00:00', b'0', NULL, NULL, 8, 2),
(24, '2023-05-08', '20:22:00', b'1', NULL, '21:23:00', 8, 2),
(25, '2023-05-06', '13:15:00', b'0', NULL, NULL, 8, 1),
(26, '2023-05-01', NULL, b'0', NULL, NULL, 8, 2),
(27, '2023-06-25', '17:00:00', b'0', NULL, NULL, 1, 2),
(28, '2023-04-24', '00:00:00', b'0', NULL, NULL, 9, 2),
(29, '2023-05-08', '20:22:00', b'1', NULL, '21:23:00', 9, 2),
(30, '2023-05-06', '13:15:00', b'0', NULL, NULL, 9, 1),
(31, '2023-05-01', NULL, b'0', NULL, NULL, 9, 2),
(32, '2023-04-10', '18:50:00', b'1', '2023-04-20', '18:50:00', 8, 2),
(33, '2023-04-20', '13:00:00', b'0', NULL, NULL, 9, 2),
(34, '2023-04-20', '14:00:00', b'0', NULL, NULL, 9, 2),
(35, '2023-04-24', '00:00:00', b'0', NULL, NULL, 10, 2),
(36, '2023-05-08', '20:22:00', b'1', NULL, '21:23:00', 10, 2),
(37, '2023-04-15', '13:15:00', b'0', NULL, '14:16:00', 10, 1),
(38, '2023-05-01', '13:15:00', b'0', NULL, '14:16:00', 10, 2),
(39, '2023-05-15', '20:00:00', b'0', NULL, NULL, 10, 3),
(40, '2023-08-28', '17:00:00', b'1', NULL, '19:00:00', 1, 1),
(41, '2023-05-24', '00:00:00', b'0', NULL, NULL, 11, 2),
(42, '2023-05-03', '20:22:00', b'1', NULL, '21:23:00', 11, 2),
(43, '2023-04-15', '13:15:00', b'0', NULL, '14:16:00', 11, 1),
(44, '2023-05-01', '13:15:00', b'0', NULL, '14:16:00', 11, 2);

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `image`
--

CREATE TABLE IF NOT EXISTS `image` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) DEFAULT NULL,
  `is_about` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

--
-- Andmete tõmmistamine tabelile `image`
--

INSERT IGNORE INTO `image` (`id`, `image`, `is_about`, `parent_id`) VALUES
(1, 'Cambridgeshire International', 'VENUE', 68738),
(2, 'indexing Ball Planner', 'VENUE', 88131),
(3, 'recontextualize Harbors', 'EVENT', 49296),
(4, 'Outdoors quantify fuchsia', 'EVENT', 99289),
(5, 'Steel', 'VENUE', 24034),
(6, 'Loan Books Pre-emptive', 'EVENT', 72015),
(7, 'hack Producer', 'EVENT', 55611),
(8, 'Mobility Thailand engine', 'EVENT', 23000),
(9, 'Virginia groupware optical', 'VENUE', 81640),
(10, 'orange Fiji Cotton', 'VENUE', 41855);

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `jhi_authority`
--

CREATE TABLE IF NOT EXISTS `jhi_authority` (
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Andmete tõmmistamine tabelile `jhi_authority`
--

INSERT IGNORE INTO `jhi_authority` (`name`) VALUES
('ROLE_ADMIN'),
('ROLE_USER');

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `jhi_date_time_wrapper`
--

CREATE TABLE IF NOT EXISTS `jhi_date_time_wrapper` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instant` timestamp NULL DEFAULT NULL,
  `local_date_time` timestamp NULL DEFAULT NULL,
  `offset_date_time` timestamp NULL DEFAULT NULL,
  `zoned_date_time` timestamp NULL DEFAULT NULL,
  `local_time` time DEFAULT NULL,
  `offset_time` time DEFAULT NULL,
  `local_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `jhi_user`
--

CREATE TABLE IF NOT EXISTS `jhi_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `login` varchar(50) NOT NULL,
  `password_hash` varchar(60) NOT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `email` varchar(191) DEFAULT NULL,
  `image_url` varchar(256) DEFAULT NULL,
  `activated` bit(1) NOT NULL,
  `lang_key` varchar(10) DEFAULT NULL,
  `activation_key` varchar(20) DEFAULT NULL,
  `reset_key` varchar(20) DEFAULT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  `reset_date` timestamp NULL DEFAULT NULL,
  `last_modified_by` varchar(50) DEFAULT NULL,
  `last_modified_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_user_login` (`login`),
  UNIQUE KEY `ux_user_email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=1053 DEFAULT CHARSET=utf8mb4;

--
-- Andmete tõmmistamine tabelile `jhi_user`
--

INSERT IGNORE INTO `jhi_user` (`id`, `login`, `password_hash`, `first_name`, `last_name`, `email`, `image_url`, `activated`, `lang_key`, `activation_key`, `reset_key`, `created_by`, `created_date`, `reset_date`, `last_modified_by`, `last_modified_date`) VALUES
(1, 'admin', '$2a$10$gSAhZrxMllrbgj/kkK9UceBPpChGWJA7SYIb1Mqo.n5aNLq1/oRrC', 'Administrator', 'Administrator', 'admin@localhost', '', b'1', 'et', NULL, NULL, 'system', NULL, NULL, 'system', NULL),
(2, 'user', '$2a$10$VEjxo0jq2YG9Rbk2HmX9S.k1uZBGYUHdUcid3g/vfiEl7lwWgOH/K', 'User', 'User', 'user@localhost', '', b'1', 'et', NULL, NULL, 'system', NULL, NULL, 'system', NULL),
(1050, 'jaanusnurmoja', '$2a$10$uG4YsU1nrVSl1R7LiCEgd.cAlyd.Dqc5YFkXRwnqYRXOVgmRDJTa2', NULL, NULL, 'jaanus@nurmoja.net.ee', NULL, b'1', 'et', 'rueqC1qb8SdTGZeDjscF', NULL, 'anonymousUser', '2023-03-25 00:09:17', NULL, 'admin', '2023-03-25 00:18:49'),
(1051, 'lihtsalt1', '$2a$10$GbjbfoMec0RvNWPaMjawruxMccGx6QGfYzSJm7HNSjiG0XLGf7qZi', NULL, NULL, 'hibou30x@hot.ee', NULL, b'1', 'et', NULL, NULL, 'jaanusnurmoja', '2023-03-25 21:24:52', NULL, 'anonymousUser', '2023-03-25 21:28:13'),
(1052, 'zahidul', '$2a$10$ZIRyzwj8J/4T2drJs9yY3u0wY6f8SKpeTEP4YPGuhlxUbHhz19AvC', 'Zahidul', 'Haque', 'zahidul@localhost', NULL, b'1', 'en', NULL, '12DI05Nv7maVDepyP1z9', 'admin', '2023-03-30 07:22:09', '2023-03-30 07:22:09', 'admin', '2023-03-30 07:22:09');

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `jhi_user_authority`
--

CREATE TABLE IF NOT EXISTS `jhi_user_authority` (
  `user_id` bigint(20) NOT NULL,
  `authority_name` varchar(50) NOT NULL,
  PRIMARY KEY (`user_id`,`authority_name`),
  KEY `fk_authority_name` (`authority_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Andmete tõmmistamine tabelile `jhi_user_authority`
--

INSERT IGNORE INTO `jhi_user_authority` (`user_id`, `authority_name`) VALUES
(1, 'ROLE_ADMIN'),
(1, 'ROLE_USER'),
(2, 'ROLE_USER'),
(1050, 'ROLE_ADMIN'),
(1051, 'ROLE_USER'),
(1052, 'ROLE_ADMIN'),
(1052, 'ROLE_USER');

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `note`
--

CREATE TABLE IF NOT EXISTS `note` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `language` varchar(255) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `event_time_and_venue_id` bigint(20) DEFAULT NULL,
  `event_time_venue_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK18bw52sy63p7a3jfhymy3j8ns` (`event_time_and_venue_id`),
  KEY `FKgo93gghn4lgkybxulkabv2jxl` (`event_time_venue_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

--
-- Andmete tõmmistamine tabelile `note`
--

INSERT IGNORE INTO `note` (`id`, `language`, `content`, `event_time_and_venue_id`, `event_time_venue_id`) VALUES
(1, 'et', 'Lisamärkus selle toimumiskorra kohta', 2, 2),
(2, 'en', 'Additional information for those who will visit this time', 2, 2),
(3, 'fr', 'Information pour tous qui vont visiter cet évenement ce jour-là', 2, 2);

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `organisation`
--

CREATE TABLE IF NOT EXISTS `organisation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `known_name` varchar(255) NOT NULL,
  `known_abbreviation_name` varchar(255) DEFAULT NULL,
  `official_registry_name` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `registry_code` varchar(255) DEFAULT NULL,
  `created` date DEFAULT NULL,
  `is_active` bit(1) DEFAULT NULL,
  `ended` date DEFAULT NULL,
  `primary_contact_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_organisation__primary_contact_id` (`primary_contact_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

--
-- Andmete tõmmistamine tabelile `organisation`
--

INSERT IGNORE INTO `organisation` (`id`, `known_name`, `known_abbreviation_name`, `official_registry_name`, `type`, `registry_code`, `created`, `is_active`, `ended`, `primary_contact_id`) VALUES
(1, 'Concert Frog', 'CF', 'OÜ Konnakontsert', 'OÜ', '12345678', '2023-03-24', b'1', NULL, 10),
(2, 'Couple Cups', '22', 'OÜ CC', 'OÜ', '12345678', '2023-03-24', b'1', NULL, 9),
(3, 'Fresh', 'Kina', 'repurpose Minnesota Chair', 'synthesizing', 'Mouse Supervisor 1080p', '2023-03-24', b'0', '2023-03-24', NULL),
(4, 'Ports Developer', 'Home', 'Lempira', 'Stream holistic proactive', 'Buckinghamshire coherent', '2023-03-24', b'0', '2023-03-24', NULL),
(5, 'Salad Reactive', 'toolset open-source SDD', 'Lithuania Cotton', 'Highway SMTP', 'SQL Toys connect', '2023-03-24', b'0', '2023-03-24', NULL),
(6, 'SSL navigating', 'capacitor silver lavender', 'Music Hryvnia deposit', 'indigo quantifying SMTP', 'Buckinghamshire neural', '2023-03-24', b'0', '2023-03-23', NULL),
(7, 'Mouse Savings', 'Branding', 'Clothing', 'deposit fuchsia', 'connecting Rustic', '2023-03-24', b'0', '2023-03-23', NULL),
(8, 'Cambridgeshire Turnpike', 'Jordan initiatives Intelligent', 'Fully-configurable Applications', 'Lao', 'quantify Chair Fantastic', '2023-03-23', b'0', '2023-03-23', NULL),
(9, 'Profound encompassing deposit', 'value-added Tennessee Cambridgeshire', 'Market', 'Cambridgeshire bricks-and-clicks', 'synthesize', '2023-03-24', b'0', '2023-03-24', NULL),
(10, 'generating Lesotho', 'compress Tools', 'South Specialist', 'payment', 'discrete', '2023-03-24', b'1', '2023-03-23', NULL);

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `person`
--

CREATE TABLE IF NOT EXISTS `person` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `middle_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) NOT NULL,
  `national_id` varchar(255) DEFAULT NULL,
  `born` date NOT NULL,
  `is_alive` bit(1) DEFAULT NULL,
  `deceased` date DEFAULT NULL,
  `primary_contact_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_person__primary_contact_id` (`primary_contact_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

--
-- Andmete tõmmistamine tabelile `person`
--

INSERT IGNORE INTO `person` (`id`, `first_name`, `middle_name`, `last_name`, `national_id`, `born`, `is_alive`, `deceased`, `primary_contact_id`) VALUES
(1, 'Jaanus', NULL, 'Nurmoja', '3670620305', '1967-06-23', b'1', NULL, NULL),
(2, 'Zahidul', NULL, 'Haque', NULL, '2001-01-01', b'1', NULL, NULL),
(3, 'Afsana', NULL, 'Afsana', NULL, '2001-01-01', b'1', NULL, NULL),
(4, 'Andrus', NULL, 'Aitsam', NULL, '2001-01-01', b'1', NULL, NULL),
(5, 'Friedrich', 'Reinhold', 'Kreutzwald', NULL, '1803-12-26', b'0', '1882-08-25', NULL),
(6, 'Tyrel', 'Central', 'Moen', '', '2023-03-24', b'1', NULL, NULL),
(7, 'Otis', NULL, 'Lynch', NULL, '2023-03-23', b'1', NULL, NULL),
(8, 'Zaria', NULL, 'Wolf', NULL, '2023-03-24', b'1', NULL, NULL),
(9, 'Martina', 'Tasty', 'Rodriguez', NULL, '2023-03-24', b'1', NULL, NULL),
(10, 'Clifford', NULL, 'Sporer', NULL, '2023-03-23', b'1', NULL, NULL);

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `primary_contact_info`
--

CREATE TABLE IF NOT EXISTS `primary_contact_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `person_or_org` varchar(255) DEFAULT NULL,
  `email_id` bigint(20) DEFAULT NULL,
  `address_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_primary_contact_info__email_id` (`email_id`),
  KEY `FK9d8kt3gcafe8iqq1wnhbyr0ky` (`address_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

--
-- Andmete tõmmistamine tabelile `primary_contact_info`
--

INSERT IGNORE INTO `primary_contact_info` (`id`, `person_or_org`, `email_id`, `address_id`) VALUES
(1, 'PERSON', 1, 3),
(2, 'PERSON', 2, 1),
(3, 'ORGANISATION', 3, 2),
(4, 'ORGANISATION', 4, 4),
(5, 'ORGANISATION', 5, 5),
(6, 'ORGANISATION', 6, 6),
(7, 'ORGANISATION', 7, 7),
(8, 'ORGANISATION', 8, 8),
(9, 'ORGANISATION', 9, 9),
(10, 'ORGANISATION', 10, 11);

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `primary_email`
--

CREATE TABLE IF NOT EXISTS `primary_email` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

--
-- Andmete tõmmistamine tabelile `primary_email`
--

INSERT IGNORE INTO `primary_email` (`id`, `email`) VALUES
(1, 'jaanus.nurmoja@gmail.com'),
(2, 'zahidul@sda.ee'),
(3, 'afsana@whatever.org'),
(4, 'Ruben_Bosco37@yahoo.com'),
(5, 'Candace.Becker59@gmail.com'),
(6, 'Daija63@hotmail.com'),
(7, 'Noe2@hotmail.com'),
(8, 'Elvie.Price75@yahoo.com'),
(9, 'aiassadassaia@gmail.com'),
(10, 'konna.kontsert@mail.ee');

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `primary_phone`
--

CREATE TABLE IF NOT EXISTS `primary_phone` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `phone_number` int(11) DEFAULT NULL,
  `formatted_number` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `primary_contact_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKek492j02qs3i8ljd4g20itisg` (`primary_contact_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

--
-- Andmete tõmmistamine tabelile `primary_phone`
--

INSERT IGNORE INTO `primary_phone` (`id`, `phone_number`, `formatted_number`, `type`, `primary_contact_id`) VALUES
(1, 65653434, '65 65 34 34', 'DESKTOP', NULL),
(2, 6502020, '650 20 20', 'DESKTOP', NULL),
(3, 5341183, '53 41 13 83', 'MOBILE', NULL),
(4, 5525287, '55 25 287', 'MOBILE', NULL),
(5, 5656656, '56 56 56 56', 'MOBILE', NULL),
(6, 6535585, '65 35 585', 'DESKTOP', NULL),
(7, 6404270, '640 42 70', 'DESKTOP', NULL),
(8, 3225887, '322 58 87', 'DESKTOP', NULL),
(9, 3225888, '322 58 88', 'DESKTOP', NULL),
(10, 3225826, '322 58 26', 'DESKTOP', NULL);

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `region`
--

CREATE TABLE IF NOT EXISTS `region` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `region_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

--
-- Andmete tõmmistamine tabelile `region`
--

INSERT IGNORE INTO `region` (`id`, `region_name`) VALUES
(1, 'Europe'),
(2, 'Asia'),
(3, 'Africa'),
(4, 'Oceania'),
(5, 'North America'),
(6, 'South America'),
(7, 'na'),
(8, 'na'),
(9, 'na'),
(10, 'na');

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `rel_fact_book_item__related`
--

CREATE TABLE IF NOT EXISTS `rel_fact_book_item__related` (
  `related_id` bigint(20) NOT NULL,
  `fact_book_item_id` bigint(20) NOT NULL,
  PRIMARY KEY (`fact_book_item_id`,`related_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Andmete tõmmistamine tabelile `rel_fact_book_item__related`
--

INSERT IGNORE INTO `rel_fact_book_item__related` (`related_id`, `fact_book_item_id`) VALUES
(2, 1),
(3, 1);

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `settlement`
--

CREATE TABLE IF NOT EXISTS `settlement` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` int(11) DEFAULT NULL,
  `short_name` varchar(255) DEFAULT NULL,
  `long_name` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `country_id` smallint(3) UNSIGNED ZEROFILL DEFAULT NULL,
  `territorial_unit_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_settlement__country_id` (`country_id`),
  KEY `fk_settlement__territorial_unit_id` (`territorial_unit_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

--
-- Andmete tõmmistamine tabelile `settlement`
--

INSERT IGNORE INTO `settlement` (`id`, `code`, `short_name`, `long_name`, `type`, `country_id`, `territorial_unit_id`) VALUES
(1, 663, 'Rakvere', 'Rakvere linn', 'linn', 233, 1),
(2, 784, 'Tallinn', 'Tallinna linn', 'city', 233, 2),
(3, 3895, 'Kärdla', 'Kärdla linn', 'town', 233, 3),
(4, 2270, 'Jõhvi', 'Jõhvi linn', 'town inside a commuity', 233, 4),
(5, 2262, 'Jõgeva', 'Jõgeva linn', 'town inside community', 233, 5);

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `territorial_unit`
--

CREATE TABLE IF NOT EXISTS `territorial_unit` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` int(11) DEFAULT NULL,
  `short_name` varchar(255) DEFAULT NULL,
  `long_name` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `country_id` smallint(3) UNSIGNED ZEROFILL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_territorial_unit__country_id` (`country_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4;

--
-- Andmete tõmmistamine tabelile `territorial_unit`
--

INSERT IGNORE INTO `territorial_unit` (`id`, `code`, `short_name`, `long_name`, `type`, `country_id`) VALUES
(1, 60, 'Lääne-Virumaa', 'Lääne-Viru Maakond', 'county', 233),
(2, 37, 'Harjumaa', 'Harju maakoond', 'county', 233),
(3, 39, 'Hiiumaa', 'Hiiu maakond', 'county', 233),
(4, 45, 'Ida-Virumaa', 'Ida-Viru maakond', 'county', 233),
(5, 50, 'Jõgevamaa', 'Jõgeva maakond', 'county', 233),
(6, 52, 'Järvamaa', 'Järva maakond', 'county', 233),
(7, 56, 'Läänemaa', 'Lääne maakond', 'county', 233),
(8, 64, 'Põlvamaa', 'Põlva maakond', 'county', 233),
(9, 68, 'Pärnumaa', 'Pärnu maakond', 'county', 233),
(10, 71, 'Raplamaa', 'Rapla maakond', 'county', 233),
(11, 74, 'Saaremaa', 'Saare maakond', 'county', 233),
(12, 79, 'Tartumaa', 'Tartu maakond', 'county', 233),
(13, 81, 'Valgamaa', 'Valga maakond', 'county', 233),
(14, 84, 'Viljandimaa', 'Viljandi maakond', 'county', 233),
(15, 87, 'Võrumaa', 'Võru maakond', 'county', 233);

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `user_person`
--

CREATE TABLE IF NOT EXISTS `user_person` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `person_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `person` (`person_id`),
  KEY `user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;

--
-- Andmete tõmmistamine tabelile `user_person`
--

INSERT IGNORE INTO `user_person` (`id`, `user_id`, `person_id`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 1050, 1),
(4, 1051, 1),
(11, 1052, 2);

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `venue`
--

CREATE TABLE IF NOT EXISTS `venue` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `address_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKh4ce7wcayhe962fxj2gd8bwq0` (`address_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

--
-- Andmete tõmmistamine tabelile `venue`
--

INSERT IGNORE INTO `venue` (`id`, `address_id`) VALUES
(2, 2),
(3, 3),
(4, 4),
(5, 5),
(6, 6),
(7, 7),
(8, 8),
(9, 9),
(10, 10),
(1, 11);

--
-- Tõmmistatud tabelite piirangud
--

--
-- Piirangud tabelile `about`
--
ALTER TABLE `about`
  ADD CONSTRAINT `FKee2rp9ilpky4yp9s6oeij1p8q` FOREIGN KEY (`venue_id`) REFERENCES `venue` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `FKrwj1pw2sfhrm1fgrv08nv5a5d` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Piirangud tabelile `address`
--
ALTER TABLE `address`
  ADD CONSTRAINT `FK6wkhgog5i9mfnacf6walvnt1n` FOREIGN KEY (`city_or_village_id`) REFERENCES `settlement` (`id`);

--
-- Piirangud tabelile `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `FKhr48nopy5aorw0ta1ii704tpu` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`);

--
-- Piirangud tabelile `country`
--
ALTER TABLE `country`
  ADD CONSTRAINT `FKs3bda8801uhqtttuaur9r6eic` FOREIGN KEY (`region_id`) REFERENCES `region` (`id`);

--
-- Piirangud tabelile `event`
--
ALTER TABLE `event`
  ADD CONSTRAINT `FKajg25so5i85r066g4bye45o24` FOREIGN KEY (`is_part_of_event_id`) REFERENCES `event` (`id`),
  ADD CONSTRAINT `FKmgnl00f5gnwobvq2r7b9ub0jh` FOREIGN KEY (`event_organizer_and_contact_id`) REFERENCES `event_organizer_and_contact` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Piirangud tabelile `event_organizer_and_contact`
--
ALTER TABLE `event_organizer_and_contact`
  ADD CONSTRAINT `FK56fuqbi252syalonydbxkojfr` FOREIGN KEY (`created_by`) REFERENCES `jhi_user` (`id`),
  ADD CONSTRAINT `FKcyvidakof5ybg3xyv0la3iso8` FOREIGN KEY (`organisation_id`) REFERENCES `organisation` (`id`);

--
-- Piirangud tabelile `event_organizer_staff`
--
ALTER TABLE `event_organizer_staff`
  ADD CONSTRAINT `FKg22s3q0nmj80tewxeu4idltto` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`),
  ADD CONSTRAINT `FKhs33jq7at09wc77fqfpyp8uv8` FOREIGN KEY (`user_id`) REFERENCES `jhi_user` (`id`),
  ADD CONSTRAINT `FKkjwgcdyj46pvh0brdek2fwg7j` FOREIGN KEY (`event_organizer_and_contact_id`) REFERENCES `event_organizer_and_contact` (`id`);

--
-- Piirangud tabelile `event_registration`
--
ALTER TABLE `event_registration`
  ADD CONSTRAINT `FK2ra3ocp6gfw2vbwybbooxxuml` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`),
  ADD CONSTRAINT `FK70hjvx9rgobx810q0lqdqs9yq` FOREIGN KEY (`user_id`) REFERENCES `jhi_user` (`id`);

--
-- Piirangud tabelile `event_time_venue`
--
ALTER TABLE `event_time_venue`
  ADD CONSTRAINT `FK47gottnr6qgjb9pq2xlri8xsk` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`),
  ADD CONSTRAINT `FKea2pdairlokrqhll5f1c9mnfy` FOREIGN KEY (`venue_id`) REFERENCES `venue` (`id`);

--
-- Piirangud tabelile `jhi_user_authority`
--
ALTER TABLE `jhi_user_authority`
  ADD CONSTRAINT `fk_authority_name` FOREIGN KEY (`authority_name`) REFERENCES `jhi_authority` (`name`),
  ADD CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `jhi_user` (`id`);

--
-- Piirangud tabelile `note`
--
ALTER TABLE `note`
  ADD CONSTRAINT `FKgo93gghn4lgkybxulkabv2jxl` FOREIGN KEY (`event_time_venue_id`) REFERENCES `event_time_venue` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Piirangud tabelile `organisation`
--
ALTER TABLE `organisation`
  ADD CONSTRAINT `FKe1i8097y2kurm7uq371p3npwc` FOREIGN KEY (`primary_contact_id`) REFERENCES `primary_contact_info` (`id`);

--
-- Piirangud tabelile `person`
--
ALTER TABLE `person`
  ADD CONSTRAINT `FK393mi9kc3augsltxdp29rftew` FOREIGN KEY (`primary_contact_id`) REFERENCES `primary_contact_info` (`id`);

--
-- Piirangud tabelile `primary_contact_info`
--
ALTER TABLE `primary_contact_info`
  ADD CONSTRAINT `FK20pcddq0crs8avbnp2qx9cah6` FOREIGN KEY (`email_id`) REFERENCES `primary_email` (`id`),
  ADD CONSTRAINT `FK9d8kt3gcafe8iqq1wnhbyr0ky` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`);

--
-- Piirangud tabelile `primary_phone`
--
ALTER TABLE `primary_phone`
  ADD CONSTRAINT `FKek492j02qs3i8ljd4g20itisg` FOREIGN KEY (`primary_contact_id`) REFERENCES `primary_contact_info` (`id`);

--
-- Piirangud tabelile `settlement`
--
ALTER TABLE `settlement`
  ADD CONSTRAINT `FKb6b2ck66tkwvhy4plt818b4w` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`),
  ADD CONSTRAINT `fk_settlement__territorial_unit_id` FOREIGN KEY (`territorial_unit_id`) REFERENCES `territorial_unit` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Piirangud tabelile `territorial_unit`
--
ALTER TABLE `territorial_unit`
  ADD CONSTRAINT `FKc7xf4jpoxla0gajvhnca852s8` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`);

--
-- Piirangud tabelile `user_person`
--
ALTER TABLE `user_person`
  ADD CONSTRAINT `person` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`),
  ADD CONSTRAINT `user` FOREIGN KEY (`user_id`) REFERENCES `jhi_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Piirangud tabelile `venue`
--
ALTER TABLE `venue`
  ADD CONSTRAINT `FKh4ce7wcayhe962fxj2gd8bwq0` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
