# Event Manager

This is the final work of the SDA java course 

Who are working on it:
* Jaanus (PM)
* Afsana
* Zahidul

Goal: an event management system which main goal is not to be a public attractive site but rather a event data API with such features like registration as a participant of event.
By default it has a jal-json output and HAL exporer available but regarding many goals it doesn't satisfy all needs. 

To see the current state:

* download and install

* create database named eventmanager and populate it with attached sql

* run it as Spring boot app, it runs under port 8081 

* open 
  - http://localhost:8081/eventmanager/eventapi/events/1/admin
  - http://localhost:8081/eventmanager/eventapi/events/1/public
  - http://localhost:8081/eventmanager/eventapi/events/1/public?language=en
  - http://localhost:8081/eventmanager/eventapi/events/1/public?language=fr
  - http://localhost:8081/eventmanager/
  - http://localhost:8081/eventmanager/?language=en
  - http://localhost:8081/eventmanager/?language=fr

default is language=et 
To be adjusted - the api url convention. ATM /events returns us the list of occurrences but /events/1 the event itself with one or more occurrences. PERHAPS it would be better /events/timeplace for the list by eventTimeVenue 

You see that 
@30.03.2021 19:38 we had a custom read-only data which is a matter of discussion as it reflects only PM-s vision about
@06.04.2021 we have an example frontend list of events grouped by occurrences (eventTimeVenue) 

All public data is filterd by language. 

To be discussed

* which elements could an event data have

* how those elements could/should be related to each other
  
* which elements of event data should be available for administrator / event creator of for public. 
