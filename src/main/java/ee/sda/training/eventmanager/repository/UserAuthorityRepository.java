package ee.sda.training.eventmanager.repository;

import ee.sda.training.eventmanager.model.UserAuthority;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface UserAuthorityRepository extends JpaRepository<UserAuthority, String>, JpaSpecificationExecutor<UserAuthority> {

}
