package ee.sda.training.eventmanager.repository;

import ee.sda.training.eventmanager.model.PrimaryEmail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface PrimaryEmailRepository extends JpaRepository<PrimaryEmail, Long>, JpaSpecificationExecutor<PrimaryEmail> {

}
