package ee.sda.training.eventmanager.repository;

import ee.sda.training.eventmanager.model.PrimaryPhone;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface PrimaryPhoneRepository extends JpaRepository<PrimaryPhone, Long>, JpaSpecificationExecutor<PrimaryPhone> {

}
