package ee.sda.training.eventmanager.repository;

import ee.sda.training.eventmanager.model.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

public interface EventRepository extends JpaRepository<Event, Long>, CrudRepository<Event, Long>, JpaSpecificationExecutor<Event> {

}
