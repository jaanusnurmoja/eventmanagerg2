package ee.sda.training.eventmanager.repository;

import ee.sda.training.eventmanager.model.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CountryRepository extends JpaRepository<Country, Short>, JpaSpecificationExecutor<Country> {

}
