package ee.sda.training.eventmanager.repository;

import ee.sda.training.eventmanager.model.EventTimeVenue;
import ee.sda.training.eventmanager.model.Note;
import ee.sda.training.eventmanager.model.enumeration.Language;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface NoteRepository extends JpaRepository<Note, Long>, JpaSpecificationExecutor<Note> {
    List<Note> findAllByEventTimeVenueAndLanguage(EventTimeVenue eventTimeVenue, Language language);
}
