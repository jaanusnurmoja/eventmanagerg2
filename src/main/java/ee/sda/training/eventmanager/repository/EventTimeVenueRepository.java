package ee.sda.training.eventmanager.repository;

import ee.sda.training.eventmanager.model.EventTimeVenue;
import ee.sda.training.eventmanager.model.enumeration.Language;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.format.annotation.DateTimeFormat;

import java.sql.Time;
import java.time.LocalDate;
import java.util.List;

public interface EventTimeVenueRepository extends JpaRepository<EventTimeVenue, Long>, JpaSpecificationExecutor<EventTimeVenue> {
    List<EventTimeVenue> findAllByStartDateGreaterThanEqualOrderByStartDateAscStartTimeAsc(@DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate localDate);
}
