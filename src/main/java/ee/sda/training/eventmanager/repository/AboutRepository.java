package ee.sda.training.eventmanager.repository;

import ee.sda.training.eventmanager.model.About;
import ee.sda.training.eventmanager.model.enumeration.Language;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface AboutRepository extends JpaRepository<About, Long>, JpaSpecificationExecutor<About> {
    List<About> findAllByEventIdAndLanguage(Long id, Language language);
    List<About> findAllByVenueIdAndLanguage(Long id, Language language);
    About getByEvent_IdAndLanguage(Long id, Language language);
    About getByVenue_IdAndLanguage(Long id, Language language);

}
