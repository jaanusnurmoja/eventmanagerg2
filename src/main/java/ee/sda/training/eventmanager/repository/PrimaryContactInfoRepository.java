package ee.sda.training.eventmanager.repository;

import ee.sda.training.eventmanager.model.PrimaryContactInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface PrimaryContactInfoRepository extends JpaRepository<PrimaryContactInfo, Long>, JpaSpecificationExecutor<PrimaryContactInfo> {

}
