package ee.sda.training.eventmanager.model.enumeration;

/**
 * The Role enumeration.
 */
public enum Role {
    MANAGER("manager"),
    CONTACTPERSON("contactPerson"),
    SPECIALCONTACT("specialContact");

    private final String value;

    Role(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
