package ee.sda.training.eventmanager.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

@Data
@Entity
@Table(name = "jhi_date_time_wrapper")
public class JhiDateTimeWrapper implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "instant")
    private Date instant;

    @Column(name = "local_date_time")
    private Date localDateTime;

    @Column(name = "offset_date_time")
    private Date offsetDateTime;

    @Column(name = "zoned_date_time")
    private Date zonedDateTime;

    @Column(name = "local_time")
    private Time localTime;

    @Column(name = "offset_time")
    private Time offsetTime;

    @Column(name = "local_date")
    private Date localDate;

}
