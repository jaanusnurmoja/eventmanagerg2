package ee.sda.training.eventmanager.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Registrating participnts of the event
 */
@Data
@Entity
@Table(name = "event_registration")
@ApiModel("Registrating participnts of the event")
public class EventRegistration implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "all_ocurrences")
    private Boolean allOcurrences;

    @Column(name = "all_sub_events")
    private Boolean allSubEvents;

    @Column(name = "approved")
    private Boolean approved;

    @ManyToOne
    @JoinColumn(name = "registering_user")
    private User registeringUser;

    @ManyToOne
    @JsonIgnoreProperties(value = {"person"})
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @JsonIgnoreProperties(
        value = { "eventOrganizerAndContact", "eventTimeAndVenues", "abouts", "eventRegistrations", "subEvents", "isPartOfEvent" },
        allowSetters = true
    )
    private Event event;

	public void setEvent(Event event) {
		// TODO Auto-generated method stub
		this.event = event;	}

}
