package ee.sda.training.eventmanager.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "jhi_user_authority")
@IdClass(UserAuthorityId.class)
public class UserAuthority implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "user_id", nullable = false)
    private Long userId;

    @Id
    @Column(name = "authority_name", nullable = false)
    private String authorityName;

}
