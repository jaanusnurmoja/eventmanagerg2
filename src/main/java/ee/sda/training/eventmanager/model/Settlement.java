package ee.sda.training.eventmanager.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Can be ie city, village, ...
 */
@Data
@Entity
@Table(name = "settlement")
@ApiModel("Can be ie city, village, ...")
public class Settlement implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "code")
    private Integer code;

    @Column(name = "short_name")
    private String shortName;

    @Column(name = "long_name")
    private String longName;

    @Column(name = "type")
    private String type;

    @JoinColumn(name = "country_id")
    @ManyToOne
    @JsonIgnoreProperties(value = { "region" }, allowSetters = true)
    private Country country;

    @JoinColumn(name = "territorial_unit_id")
    @ManyToOne
    private TerritorialUnit territorialUnit;

}
