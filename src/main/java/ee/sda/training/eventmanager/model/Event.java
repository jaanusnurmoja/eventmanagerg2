package ee.sda.training.eventmanager.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import ee.sda.training.eventmanager.model.enumeration.EventType;
import io.swagger.annotations.ApiModel;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Main event class. Can be multidimensional as some events are consisting of multiple subevents
 */
@Entity
@Table(name = "event")
@ApiModel(description = "Main event class. Can be multidimensional as some events are consisting of multiple subevents")
public class Event implements Serializable {


    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private EventType type;

    @Column(name = "tag")
    private String tag;

    @ManyToOne
    @JsonIgnoreProperties(
            value = { "eventOrganizerAndContact", "eventTimeVenues", "abouts", "eventRegistrations", "subEvents", "isPartOfEvent" },
            allowSetters = true
    )
    private Event isPartOfEvent;

    @OneToMany(mappedBy = "event", cascade = CascadeType.ALL)
    @JsonIgnoreProperties(value = { "event", "venue" }, allowSetters = true)
    private List<About> abouts;

    @OneToMany(mappedBy = "event", cascade = CascadeType.ALL)
    @JsonIgnoreProperties(value = { "event" }, allowSetters = true)
    private List<EventTimeVenue> eventTimeVenues;

    @JsonIgnoreProperties(value = { "event" }, allowSetters = true)
    @OneToOne
    @JoinColumn(unique = true, name = "event_organizer_and_contact_id")
    private EventOrganizerAndContact eventOrganizerAndContact;

    @OneToMany(mappedBy = "event")
    @JsonIgnoreProperties(value = { "user", "event" }, allowSetters = true)
    private List<EventRegistration> eventRegistrations;

    @OneToMany(mappedBy = "isPartOfEvent")
    @JsonIgnoreProperties(
            value = { "eventOrganizerAndContact", "eventRegistrations", "subEvents", "isPartOfEvent" },
            allowSetters = true
    )
    private List<Event> subEvents;


    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Event id(Long id) {
        this.id = id;
        return this;
    }

    public EventType getType() {
        return this.type;
    }

    public Event type(EventType type) {
        this.type = type;
        return this;
    }

    public void setType(EventType type) {
        this.type = type;
    }

    public String getTag() {
        return this.tag;
    }

    public Event tag(String tag) {
        this.tag = tag;
        return this;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public EventOrganizerAndContact getEventOrganizerAndContact() {
        return this.eventOrganizerAndContact;
    }

    public Event eventOrganizerAndContact(EventOrganizerAndContact eventOrganizerAndContact) {
        this.setEventOrganizerAndContact(eventOrganizerAndContact);
        return this;
    }

    public void setEventOrganizerAndContact(EventOrganizerAndContact eventOrganizerAndContact) {
        this.eventOrganizerAndContact = eventOrganizerAndContact;
    }

    public List<EventTimeVenue> getEventTimeVenues() {
        return this.eventTimeVenues;
    }

    public Event eventTimeAndVenues(List<EventTimeVenue> eventTimeVenues) {
        this.setEventTimeVenues(eventTimeVenues);
        return this;
    }

    public Event addEventTimeAndVenue(EventTimeVenue eventTimeVenue) {
        this.eventTimeVenues.add(eventTimeVenue);
        eventTimeVenue.setEvent(this);
        return this;
    }

    public Event removeEventTimeAndVenue(EventTimeVenue eventTimeVenue) {
        this.eventTimeVenues.remove(eventTimeVenue);
        eventTimeVenue.setEvent(null);
        return this;
    }

    public void setEventTimeVenues(List<EventTimeVenue> eventTimeVenues) {
        if (this.eventTimeVenues != null) {
            this.eventTimeVenues.forEach(i -> i.setEvent(null));
        }
        if (eventTimeVenues != null) {
            eventTimeVenues.forEach(i -> i.setEvent(this));
        }
        this.eventTimeVenues = eventTimeVenues;
    }

    public List<About> getAbouts() {
        return this.abouts;
    }

    public Event abouts(List<About> abouts) {
        this.setAbouts(abouts);
        return this;
    }

    public Event addAbout(About about) {
        this.abouts.add(about);
        about.setEvent(this);
        return this;
    }

    public Event removeAbout(About about) {
        this.abouts.remove(about);
        about.setEvent(null);
        return this;
    }

    public void setAbouts(List<About> abouts) {
        if (this.abouts != null) {
            this.abouts.forEach(i -> i.setEvent(null));
        }
        if (abouts != null) {
            abouts.forEach(i -> i.setEvent(this));
        }
        this.abouts = abouts;
    }

    public List<EventRegistration> getEventRegistrations() {
        return this.eventRegistrations;
    }

    public Event eventRegistrations(List<EventRegistration> eventRegistrations) {
        this.setEventRegistrations(eventRegistrations);
        return this;
    }

    public Event addEventRegistration(EventRegistration eventRegistration) {
        this.eventRegistrations.add(eventRegistration);
        eventRegistration.setEvent(this);
        return this;
    }

    public Event removeEventRegistration(EventRegistration eventRegistration) {
        this.eventRegistrations.remove(eventRegistration);
        eventRegistration.setEvent(null);
        return this;
    }

    public void setEventRegistrations(List<EventRegistration> eventRegistrations) {
        if (this.eventRegistrations != null) {
            this.eventRegistrations.forEach(i -> i.setEvent(null));
        }
        if (eventRegistrations != null) {
            eventRegistrations.forEach(i -> i.setEvent(this));
        }
        this.eventRegistrations = eventRegistrations;
    }

    public List<Event> getSubEvents() {
        return this.subEvents;
    }

    public Event subEvents(List<Event> events) {
        this.setSubEvents(events);
        return this;
    }

    public Event addSubEvents(Event event) {
        this.subEvents.add(event);
        event.setIsPartOfEvent(this);
        return this;
    }

    public Event removeSubEvents(Event event) {
        this.subEvents.remove(event);
        event.setIsPartOfEvent(null);
        return this;
    }

    public void setSubEvents(List<Event> events) {
        if (this.subEvents != null) {
            this.subEvents.forEach(i -> i.setIsPartOfEvent(null));
        }
        if (events != null) {
            events.forEach(i -> i.setIsPartOfEvent(this));
        }
        this.subEvents = events;
    }

    public Event getIsPartOfEvent() {
        return this.isPartOfEvent;
    }

    public Event isPartOfEvent(Event event) {
        this.setIsPartOfEvent(event);
        return this;
    }

    public void setIsPartOfEvent(Event event) {
        this.isPartOfEvent = event;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Event)) {
            return false;
        }
        return id != null && id.equals(((Event) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Event{" +
            "id=" + getId() +
            ", type='" + getType() + "'" +
            ", tag='" + getTag() + "'" +
            "}";
    }
}
