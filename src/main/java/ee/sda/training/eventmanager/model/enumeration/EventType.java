package ee.sda.training.eventmanager.model.enumeration;

/**
 * The EventType enumeration.
 */
public enum EventType {
    REGULAR,
    MULTIPART,
    PART,
}
