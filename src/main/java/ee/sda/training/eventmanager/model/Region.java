package ee.sda.training.eventmanager.model;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Global region, e.g Europe, North America, ...
 */
@Data
@Entity
@Table(name = "region")
@ApiModel("Global region, e.g Europe, North America, ...")
public class Region implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "region_name")
    private String regionName;

}
