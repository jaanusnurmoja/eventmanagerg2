package ee.sda.training.eventmanager.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Primary email of a person or organisation
 */
@Data
@Entity
@Table(name = "primary_email")
@ApiModel("Primary email of a person or organisation")
public class PrimaryEmail implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "email")
    private String email;

    @JsonIgnoreProperties(value = { "email", "primaryPhones", "address", "ownerPerson", "ownerOrg" }, allowSetters = true)
    @OneToOne(mappedBy = "email")
    private PrimaryContactInfo primaryContact;

}
