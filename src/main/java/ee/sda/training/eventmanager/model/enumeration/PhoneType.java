package ee.sda.training.eventmanager.model.enumeration;

/**
 * The PhoneType enumeration.
 */
public enum PhoneType {
    MOBILE,
    DESKTOP,
}
