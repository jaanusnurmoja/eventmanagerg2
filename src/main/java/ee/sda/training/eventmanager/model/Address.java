package ee.sda.training.eventmanager.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Street name, house num and postal code. Settlement and other attributes in related tables
 */
@Data
@Entity
@Table(name = "address")
@ApiModel("Street name, house num and postal code. Settlement and other attributes in related tables")
public class Address implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "street_name")
    private String streetName;

    @Column(name = "house_number")
    private String houseNumber;

    @Column(name = "app_number")
    private String appNumber;

    @Column(name = "postal_code")
    private String postalCode;

    @ManyToOne
    @JoinColumn(name = "country_id")
    @JsonIgnoreProperties(value = { "region" }, allowSetters = true)
    private Country country;

    @ManyToOne
    @JoinColumn(name = "city_or_village_id")
    @JsonIgnoreProperties(value = { "country" }, allowSetters = true)
    private Settlement cityOrVillage;
}
