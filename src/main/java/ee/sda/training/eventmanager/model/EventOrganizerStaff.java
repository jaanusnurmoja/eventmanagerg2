package ee.sda.training.eventmanager.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import ee.sda.training.eventmanager.model.enumeration.Role;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Staff at the organizer
 */
@Getter
@Setter
@Entity
@ApiModel("Staff at the organizer")
@Table(name = "event_organizer_staff")
public class EventOrganizerStaff implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "email")
    private String email;

    @Column(name = "phone")
    private Integer phone;

    @Enumerated(EnumType.STRING)
    @Column(name = "role")
    private Role role;

    @ManyToOne
    @JoinColumn(name = "event_organizer_and_contact_id")
    //@JsonBackReference
    @JsonIgnoreProperties(value = { "eventOrganizerStaffs", "event" }, allowSetters = true)
    private EventOrganizerAndContact eventOrganizerAndContact;

    @ManyToOne
    @JoinColumn(name = "person_id")
    @JsonIgnoreProperties("primaryContact")
    private Person person;

}
