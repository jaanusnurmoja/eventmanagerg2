package ee.sda.training.eventmanager.model.enumeration;

/**
 * The IsAbout enumeration.
 */
public enum IsAbout {
    EVENT("event"),
    VENUE("venue");

    private final String value;

    IsAbout(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
