package ee.sda.training.eventmanager.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import ee.sda.training.eventmanager.model.enumeration.IsAbout;
import ee.sda.training.eventmanager.model.enumeration.Language;
import io.swagger.annotations.ApiModel;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Class for textual and multilingual descriptions
 */
@Entity
@Table(name = "about")
@ApiModel("Class for textual and multilingual descriptions")
public class About implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    @Lob
    private String description;

    @Enumerated(EnumType.STRING)
    @Column(name = "language")
    private Language language;

    @Enumerated(EnumType.STRING)
    @Column(name = "is_about")
    private IsAbout isAbout;

    @Column(name = "parent_id")
    private Integer parentId;

    // may I ask will it be possible "conditional relation" depending on the value of IsAbount (Event or Venue)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "event_id")
    //@JsonBackReference
    @JsonIgnoreProperties(
        value = { "eventOrganizerAndContact", "eventTimeAndVenues", "abouts", "eventRegistrations", "subEvents", "isPartOfEvent" },
        allowSetters = true
    )
    private Event event;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "venue_id")
    //@JsonBackReference
    @JsonIgnoreProperties(value = { "abouts", "address" }, allowSetters = true)
    private Venue venue;

    public About() {
    }

    public Long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getDescription() {
        return this.description;
    }

    public Language getLanguage() {
        return this.language;
    }

    public IsAbout getIsAbout() {
        return this.isAbout;
    }

    public Integer getParentId() {
        return this.parentId;
    }

    public Event getEvent() {
        return this.event;
    }

    public Venue getVenue() {
        return this.venue;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public void setIsAbout(IsAbout isAbout) {
        this.isAbout = isAbout;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    @JsonIgnoreProperties(value = {"eventOrganizerAndContact", "eventTimeAndVenues", "abouts", "eventRegistrations", "subEvents", "isPartOfEvent"}, allowSetters = true)
    public void setEvent(Event event) {
        this.event = event;
    }

    @JsonIgnoreProperties(value = {"abouts", "address"}, allowSetters = true)
    public void setVenue(Venue venue) {
        this.venue = venue;
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof About)) return false;
        final About other = (About) o;
        if (!other.canEqual((Object) this)) return false;
        final Object this$id = this.getId();
        final Object other$id = other.getId();
        if (this$id == null ? other$id != null : !this$id.equals(other$id)) return false;
        final Object this$name = this.getName();
        final Object other$name = other.getName();
        if (this$name == null ? other$name != null : !this$name.equals(other$name)) return false;
        final Object this$description = this.getDescription();
        final Object other$description = other.getDescription();
        if (this$description == null ? other$description != null : !this$description.equals(other$description))
            return false;
        final Object this$language = this.getLanguage();
        final Object other$language = other.getLanguage();
        if (this$language == null ? other$language != null : !this$language.equals(other$language)) return false;
        final Object this$isAbout = this.getIsAbout();
        final Object other$isAbout = other.getIsAbout();
        if (this$isAbout == null ? other$isAbout != null : !this$isAbout.equals(other$isAbout)) return false;
        final Object this$parentId = this.getParentId();
        final Object other$parentId = other.getParentId();
        if (this$parentId == null ? other$parentId != null : !this$parentId.equals(other$parentId)) return false;
        final Object this$event = this.getEvent();
        final Object other$event = other.getEvent();
        if (this$event == null ? other$event != null : !this$event.equals(other$event)) return false;
        final Object this$venue = this.getVenue();
        final Object other$venue = other.getVenue();
        if (this$venue == null ? other$venue != null : !this$venue.equals(other$venue)) return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof About;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $id = this.getId();
        result = result * PRIME + ($id == null ? 43 : $id.hashCode());
        final Object $name = this.getName();
        result = result * PRIME + ($name == null ? 43 : $name.hashCode());
        final Object $description = this.getDescription();
        result = result * PRIME + ($description == null ? 43 : $description.hashCode());
        final Object $language = this.getLanguage();
        result = result * PRIME + ($language == null ? 43 : $language.hashCode());
        final Object $isAbout = this.getIsAbout();
        result = result * PRIME + ($isAbout == null ? 43 : $isAbout.hashCode());
        final Object $parentId = this.getParentId();
/*
        result = result * PRIME + ($parentId == null ? 43 : $parentId.hashCode());
        final Object $event = this.getEvent();
        result = result * PRIME + ($event == null ? 43 : $event.hashCode());
        final Object $venue = this.getVenue();
        result = result * PRIME + ($venue == null ? 43 : $venue.hashCode());
*/
        return result;
    }

    public String toString() {
        return "About(id=" + this.getId() + ", name=" + this.getName() + ", description=" + this.getDescription() + ", language=" + this.getLanguage() + ", isAbout=" + this.getIsAbout() + ", parentId=" + this.getParentId() + ", event=" + this.getEvent().toString() + ", venue=" + this.getVenue().toString() + ")";
    }
}
