package ee.sda.training.eventmanager.model;

import ee.sda.training.eventmanager.model.enumeration.IsAbout;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Images to illustrate the event. Intended to be used also like an online album
 */
@Data
@Entity
@Table(name = "image")
@ApiModel("Images to illustrate the event. Intended to be used also like an online album")
public class Image implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "image")
    private String image;

    @Enumerated(EnumType.STRING)
    @Column(name = "is_about")
    private IsAbout isAbout;

    @Column(name = "parent_id")
    private Integer parentId;

}
