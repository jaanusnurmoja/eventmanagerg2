package ee.sda.training.eventmanager.model.enumeration;

/**
 * The PersonOrOrganisation enumeration.
 */
public enum PersonOrOrganisation {
    PERSON,
    ORGANISATION,
}
