package ee.sda.training.eventmanager.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import ee.sda.training.eventmanager.config.SqlTimeDeserializer;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Time;
import java.time.LocalDate;
import java.util.List;


/**
 * can be multiple per event
 */
@Data
@Entity
@Table(name = "event_time_venue")
@ApiModel("can be multiple per event")
public class EventTimeVenue implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "start_date")
    private LocalDate startDate;

    @JsonFormat(pattern = "HH:mm")
    @JsonDeserialize(using = SqlTimeDeserializer.class)
    @Column(name = "start_time")
    private Time startTime;

    @Column(name = "end_time_determined")
    private Boolean endTimeDetermined;

    @Column(name = "end_date")
    private LocalDate endDate;

    @JsonFormat(pattern = "HH:mm")
    @JsonDeserialize(using = SqlTimeDeserializer.class)
    @Column(name = "end_time")
    private Time endTime;

    @JoinColumn(name = "event_id")
    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "eventTimeVenues", "eventOrganizerAndContact", "subevents", "eventRegistrations" }, allowSetters = true)
    private Event event;

    @JsonIgnoreProperties(value = { "event" }, allowSetters = true)
    @JoinColumn(name = "venue_id")
    @ManyToOne
    private Venue venue;

    @JsonIgnoreProperties(value = { "eventTimeVenue" }, allowSetters = true)
    @OneToMany(mappedBy = "eventTimeVenue", cascade = CascadeType.ALL)
    private List<Note> notes;
}
