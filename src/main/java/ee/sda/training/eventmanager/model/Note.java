package ee.sda.training.eventmanager.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import ee.sda.training.eventmanager.model.enumeration.Language;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Smaller textual information, can be attached to the event time and venue
 */
@Data
@Entity
@Table(name = "note")
@ApiModel("Smaller textual information, can be attached to the event time and venue")
public class Note implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "language")
    private Language language;

    @Column(name = "content")
    @Lob
    private String content;

    @ManyToOne(fetch = FetchType.LAZY)
    //@JsonBackReference
    @JoinColumn(name = "event_time_venue_id")
    @JsonIgnoreProperties(value = { "venue", "event" }, allowSetters = true)
    EventTimeVenue eventTimeVenue;

}
