package ee.sda.training.eventmanager.model;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Will  be with UN country codes in id field
 */
@Data
@Entity
@Table(name = "country")
@ApiModel("Will  be with UN country codes in id field")
public class Country implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", length=3, columnDefinition = "UNSIGNED ZEROFILL SMALLINT(3)")
    private Short id;

    @Column(name = "iso_2")
    private String iso2;

    @Column(name = "short_name")
    private String shortName;

    @Column(name = "long_name")
    private String longName;

    @Column(name = "iso_3")
    private String iso3;

    @Column(name = "numcode")
    private String numcode;

    @Column(name = "un_member")
    private String unMember;

    @Column(name = "calling_code")
    private String callingCode;

    @Column(name = "cctld")
    private String cctld;

    @ManyToOne
    @JoinColumn(name = "region_id")
    private Region region;

}
