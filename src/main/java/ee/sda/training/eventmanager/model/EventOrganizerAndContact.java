package ee.sda.training.eventmanager.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Data about event organizers
 */
@Getter
@Setter
@Entity
@ApiModel("Data about event organizers ")
@Table(name = "event_organizer_and_contact")
public class EventOrganizerAndContact implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "organisation_id")
    private Organisation organisation;

    @OneToMany(mappedBy = "eventOrganizerAndContact", cascade = CascadeType.ALL)
    @JsonIgnoreProperties(value = { "eventOrganizerAndContact" }, allowSetters = true)
    private List<EventOrganizerStaff> eventOrganizerStaffs;

    @JsonIgnoreProperties(
        value = { "eventOrganizerAndContact", "eventTimeAndVenues", "abouts", "eventRegistrations", "subEvents", "isPartOfEvent" },
        allowSetters = true
    )
    @OneToOne(mappedBy = "eventOrganizerAndContact")
    private Event event;

    @ManyToOne
    @JoinColumn(name = "created_by")
    @JsonIgnoreProperties(value = {
            "firstName",
            "lastName",
            "resetDate",
            "activated",
            "langKey",
            "imageUrl"
    })
    User createdBy;

	public Organisation getOrganisation() {
		// TODO Auto-generated method stub
		return this.organisation;
	}
}
