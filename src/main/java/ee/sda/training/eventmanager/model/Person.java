package ee.sda.training.eventmanager.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.sql.Date;
import java.util.List;

/**
 * Data source of persons
 */
@Data
@Entity
@Table(name = "person")
@ApiModel("Data source of persons")
public class Person implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "middle_name")
    private String middleName;

    @NotNull
    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(name = "national_id")
    private String nationalId;

    @NotNull
    @Column(name = "born", nullable = false)
    private LocalDate born;

    @Column(name = "is_alive")
    private Boolean isAlive;

    @Column(name = "deceased")
    private Date deceased;

    @JsonIgnoreProperties(value = { "email", "primaryPhones", "address", "ownerPerson", "ownerOrg" }, allowSetters = true)
    @OneToOne
    @JoinColumn(unique = true)
    private PrimaryContactInfo primaryContact;

    @OneToMany(mappedBy = "person")
    @JsonIgnoreProperties(value = { "id", "person" })
    private List<UserPerson> userPerson;

}
