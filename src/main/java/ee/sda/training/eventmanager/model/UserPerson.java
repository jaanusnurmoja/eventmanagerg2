package ee.sda.training.eventmanager.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "user_person")
public class UserPerson implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    @JsonIgnore
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JoinColumn(name = "user_id", nullable = false, unique = true)
    @JsonIgnoreProperties(value={
            "firstName",
            "lastName",
            "email",
            "imageUrl",
            "resetDate",
            "activated",
            "langKey",
            "userPerson"

    })
    private User user;

    @ManyToOne
    @JsonIgnoreProperties(value={"primaryContactInfo", "userPerson"})
    @JoinColumn(name = "person_id", nullable = false)
    private Person person;

}
