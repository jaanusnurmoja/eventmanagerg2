package ee.sda.training.eventmanager.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import ee.sda.training.eventmanager.model.enumeration.PersonOrOrganisation;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Person or organisation related contact data
 */
@Getter
@Setter
@Entity
@Table(name = "primary_contact_info")
@ApiModel("Person or organisation related contact data")
public class PrimaryContactInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "person_or_org")
    private PersonOrOrganisation personOrOrg;

    @JsonIgnoreProperties(value = { "primaryContact" }, allowSetters = true)
    @OneToOne
    @JoinColumn(unique = true)
    private PrimaryEmail email;

    @OneToMany(mappedBy = "primaryContact")
    @JsonIgnoreProperties(value = { "primaryContact" }, allowSetters = true)
    private Set<PrimaryPhone> primaryPhones = new HashSet<>();

    @ManyToOne
    private Address address;

    @JsonIgnoreProperties(value = { "primaryContact" }, allowSetters = true)
    @OneToOne(mappedBy = "primaryContact")
    private Person ownerPerson;

    @JsonIgnoreProperties(value = { "primaryContact" }, allowSetters = true)
    @OneToOne(mappedBy = "primaryContact")
    private Organisation ownerOrg;

}
