package ee.sda.training.eventmanager.config;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.sql.Time;
/**
Jaanus: added split with length check because when the time was loaded from db into form
 then its format was correct and when posted back without changing the date field in form
 then it was deserialized as i.e 23:00:00:00
 */
public class SqlTimeDeserializer extends JsonDeserializer<Time> {
    @Override
    public Time deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
        if (jp.getValueAsString().split(":").length == 2) {
            return Time.valueOf(jp.getValueAsString() + ":00");
        }
        else {
            return Time.valueOf(jp.getValueAsString());
        }
    }
}
