package ee.sda.training.eventmanager.service;

import ee.sda.training.eventmanager.model.Event;
import ee.sda.training.eventmanager.model.EventTimeVenue;
import ee.sda.training.eventmanager.model.enumeration.Language;
import ee.sda.training.eventmanager.repository.AboutRepository;
import ee.sda.training.eventmanager.repository.EventRepository;
import ee.sda.training.eventmanager.repository.EventTimeVenueRepository;
import ee.sda.training.eventmanager.repository.NoteRepository;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class EventServiceImpl implements EventService {
    AboutRepository aboutRepository;
    NoteRepository noteRepository;
    EventRepository eventRepository;
    EventTimeVenueRepository eventTimeVenueRepository;
    public EventServiceImpl(
            AboutRepository aboutRepository,
            NoteRepository noteRepository,
            EventRepository eventRepository,
            EventTimeVenueRepository eventTimeVenueRepository
            ) {
        this.aboutRepository = aboutRepository;
        this.noteRepository = noteRepository;
        this.eventRepository = eventRepository;
        this.eventTimeVenueRepository = eventTimeVenueRepository;
    }


    public List<Event> findAllByLanguage(String language) {
        Language ln = language == null ?  Language.et : Language.valueOf(language);
        List<Event> eventList = eventRepository.findAll();
        eventList.forEach(event -> {
            event.setAbouts(aboutRepository.findAllByEventIdAndLanguage(event.getId(), ln));
            event.getEventTimeVenues().forEach(occurrence ->
            {
                occurrence.getVenue().setAbouts(aboutRepository.findAllByVenueIdAndLanguage(
                        occurrence.getVenue().getId(), ln));
                if (occurrence.getNotes() != null) occurrence.setNotes(noteRepository.findAllByEventTimeVenueAndLanguage(occurrence, ln));
            });
        });
        return eventList;
    }

    public Optional<Event> findByIdAndLanguage(Long id, String language) {
        Optional<Event> event = eventRepository.findById(id);
        Language ln = language == null ?  Language.et : Language.valueOf(language);
        event.ifPresent(value -> value.setAbouts(aboutRepository.findAllByEventIdAndLanguage(id, ln)));
        event.ifPresent(value -> value.getEventTimeVenues().forEach(
                occurrence -> {
                    occurrence
                            .getVenue().setAbouts(aboutRepository.findAllByVenueIdAndLanguage(
                            occurrence.getVenue().getId(), ln));
                    occurrence.setNotes(noteRepository.findAllByEventTimeVenueAndLanguage(occurrence, ln));
                }
        ));

        return event;
    }

    public Event publicDetails(Long id, String language) {
        Event details = new Event();
        if (findByIdAndLanguage(id, language).isPresent()) {
            details = findByIdAndLanguage(id, language).get();
            details.setEventRegistrations(null);
            details.getEventOrganizerAndContact().setCreatedBy(null);
            details.getEventOrganizerAndContact().getEventOrganizerStaffs().forEach(member ->{
                member.getPerson().setUserPerson(null);
            });
        }
        return details;
    }

    public List<EventTimeVenue> findAllUpcomingByLanguage(String language, @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate localDate) {

        List<EventTimeVenue> eventsByTimeVenue = eventTimeVenueRepository.findAllByStartDateGreaterThanEqualOrderByStartDateAscStartTimeAsc(localDate);
       Language ln = language == null ? Language.et : Language.valueOf(language);
        eventsByTimeVenue.forEach(eventTimeVenue -> {
            eventTimeVenue.getEvent()
                    .setAbouts(aboutRepository.findAllByEventIdAndLanguage(
                            eventTimeVenue.getEvent().getId(), ln));
            eventTimeVenue.getVenue()
                    .setAbouts(aboutRepository
                            .findAllByVenueIdAndLanguage(eventTimeVenue.getVenue().getId(), ln));

        });
        return eventsByTimeVenue;
    }

    public EventTimeVenue getOneByIdAndLanguage(Long id, String language) {
        EventTimeVenue eventTimeVenueById = eventTimeVenueRepository.getOne(id);
        Language ln = language == null ? Language.et : Language.valueOf(language);
        eventTimeVenueById.getEvent()
                .setAbouts(aboutRepository.findAllByEventIdAndLanguage(
                        eventTimeVenueById.getEvent().getId(), ln));
        eventTimeVenueById.getVenue()
                .setAbouts(aboutRepository
                        .findAllByVenueIdAndLanguage(eventTimeVenueById.getVenue().getId(), ln));
        if (eventTimeVenueById.getNotes() != null)
            eventTimeVenueById.setNotes(noteRepository.findAllByEventTimeVenueAndLanguage(eventTimeVenueById, ln));

        return eventTimeVenueById;
    }
}

