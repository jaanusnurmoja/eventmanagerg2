package ee.sda.training.eventmanager.service;

import ee.sda.training.eventmanager.model.Event;
import ee.sda.training.eventmanager.model.EventTimeVenue;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface EventService {
    Optional<Event> findByIdAndLanguage(Long id, String language);
    List<Event> findAllByLanguage(String language);
    Event publicDetails(Long id, String language);
    List<EventTimeVenue> findAllUpcomingByLanguage(String language, @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate now);
    EventTimeVenue getOneByIdAndLanguage(Long id, String language);
}
