package ee.sda.training.eventmanager.controller;

import ee.sda.training.eventmanager.model.Event;
import ee.sda.training.eventmanager.model.EventTimeVenue;
import ee.sda.training.eventmanager.model.Venue;
import ee.sda.training.eventmanager.repository.AboutRepository;
import ee.sda.training.eventmanager.repository.EventRepository;
import ee.sda.training.eventmanager.repository.EventTimeVenueRepository;
import ee.sda.training.eventmanager.repository.VenueRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import java.net.URL;

@RestController
@RequiredArgsConstructor
@RequestMapping("/")
public class EventTimeVenueRestController {
    private final EventTimeVenueRepository eventTimeVenueRepository;
    private final EventRepository eventRepository;
    private final VenueRepository venueRepository;


    @PostMapping("/data/api/eventTimeVenues/add")
    String createTimeVenue(@RequestBody EventTimeVenue eventTimeVenue) {
        Event event = eventRepository.getOne(eventTimeVenue.getEvent().getId());
        Venue venue = venueRepository.getOne(eventTimeVenue.getVenue().getId());
        eventTimeVenue.setEvent(event);
        eventTimeVenue.setVenue(venue);
        eventTimeVenueRepository.save(eventTimeVenue);
        return "/adminevent/" + eventTimeVenue.getEvent().getId();
    }

    @RequestMapping(value = "/data/api/eventTimeVenues/{id}/update", method = {RequestMethod.PUT,RequestMethod.PATCH})
    String updateTimeVenue(@RequestBody EventTimeVenue eventTimeVenue) {
        Event event = eventRepository.getOne(eventTimeVenue.getEvent().getId());
        Venue venue = venueRepository.getOne(eventTimeVenue.getVenue().getId());
        eventTimeVenue.setEvent(event);
        eventTimeVenue.setVenue(venue);
        eventTimeVenueRepository.save(eventTimeVenue);
        return "/adminevent/" + eventTimeVenue.getEvent().getId();
    }

}
