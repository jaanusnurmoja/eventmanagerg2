package ee.sda.training.eventmanager.controller;

import ee.sda.training.eventmanager.model.Event;
import ee.sda.training.eventmanager.model.EventTimeVenue;
import ee.sda.training.eventmanager.model.enumeration.Language;
import ee.sda.training.eventmanager.repository.AboutRepository;
import ee.sda.training.eventmanager.repository.EventRepository;
import ee.sda.training.eventmanager.repository.EventTimeVenueRepository;
import ee.sda.training.eventmanager.service.EventService;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDate;
import java.util.List;

@Controller
@RequiredArgsConstructor
@RequestMapping("")
public class EventController {
    //Get event list

    final private EventService eventService;
    final private EventRepository eventRepository;
    final private EventTimeVenueRepository eventTimeVenueRepository;
    final private AboutRepository aboutRepository;

    /**
        Events are listed by occurrences and filtered also by language (default-et)
    */
    @GetMapping({ "/"})
    ModelAndView getAllEventsByOccurrence(
            @RequestParam(value = "language", required = false) String language,
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate localDate
    ) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("index");
        modelAndView.addObject("title", "Events: ");
        localDate = LocalDate.now();
        modelAndView.addObject("language", language);
        if (language == null) language = "et";
        modelAndView.addObject("occurrences", eventService.findAllUpcomingByLanguage(language, localDate));
        return modelAndView;
    }


    @GetMapping("/adminevent/{id}")
    ModelAndView eventAdminDashboard(@PathVariable("id") Long id) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("adminevent");
        if (eventRepository.findById(id).isPresent())
        modelAndView.addObject("event", eventRepository.getOne(id));
        return modelAndView;
    }

    @GetMapping("/adminevent/{id}/about/{language}/edit")
    String editAbout(@PathVariable("id") Long id,
                     @PathVariable("language") String language,
                     Model model) {
        Language ln = Language.valueOf(language);
        model.addAttribute("about", aboutRepository.getByEvent_IdAndLanguage(id, ln));
        return "editaboutornote";
    }

    @GetMapping("/event/{id}")
    ModelAndView eventPublicView(@PathVariable("id") Long id,
                                 @RequestParam(value = "language", required = false) String language) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("event");
            modelAndView.addObject("publicevent", eventService.publicDetails(id, language));
        return modelAndView;
    }

    @GetMapping("/whenwhere/{id}")
    ModelAndView eventOccurrence(
            @PathVariable("id") Long id,
            @RequestParam(value = "language", required = false) String language
    )
    {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("whenwhere");
        if (eventTimeVenueRepository.findById(id).isPresent())
            modelAndView.addObject("occurrence", eventService.getOneByIdAndLanguage(id, language));
        return modelAndView;
    }

    @GetMapping("/adminevent/new")
    String addEvent(Model model) {
        model.addAttribute("event", new Event());
        return "newevent";
    }

}
