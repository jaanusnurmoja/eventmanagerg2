package ee.sda.training.eventmanager.controller;

import ee.sda.training.eventmanager.model.Event;
import ee.sda.training.eventmanager.model.EventTimeVenue;
import ee.sda.training.eventmanager.model.Venue;
import ee.sda.training.eventmanager.model.enumeration.Language;
import ee.sda.training.eventmanager.repository.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;

@Controller
@RequiredArgsConstructor
@RequestMapping("/")
public class EventTimeVenueController {
    private final EventTimeVenueRepository eventTimeVenueRepository;
    private final EventRepository eventRepository;
    private final VenueRepository venueRepository;
    private final AboutRepository aboutRepository;

    @GetMapping("/adminevent/{eventId}/whenwhere/new")
    String addTimeVenue(@PathVariable("eventId") Long eventId, Model model) {
        Event event = eventRepository.getOne(eventId);
        EventTimeVenue whenWhere = new EventTimeVenue();
        List<Venue> venueList = venueRepository.findAll();
        venueList.forEach(venue -> venue.setAbouts(aboutRepository.findAllByVenueIdAndLanguage(venue.getId(), Language.et)));
        whenWhere.setEvent(event);
        model.addAttribute("whenwhere", whenWhere);
        model.addAttribute("venuelist", venueList);
        return "newwhenwhere";
    }

    @GetMapping("/adminevent/{eventId}/whenwhere/{id}/edit")
    String editTimeVenue(@PathVariable("eventId") Long eventId, @PathVariable("id") Long id, Model model) {
        model.addAttribute("event", eventRepository.getOne(eventId));
        model.addAttribute("whenwhere", eventTimeVenueRepository.getOne(id));
        model.addAttribute("selectedvenue", venueRepository.getOne(eventTimeVenueRepository.getOne(id).getVenue().getId()));
        List<Venue> venueList = venueRepository.findAll();
        venueList.forEach(venue -> venue.setAbouts(aboutRepository.findAllByVenueIdAndLanguage(venue.getId(), Language.et)));
        model.addAttribute("venuelist", venueList);
        return "editwhenwhere";
    }

}
