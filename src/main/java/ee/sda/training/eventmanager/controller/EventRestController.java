package ee.sda.training.eventmanager.controller;

import ee.sda.training.eventmanager.model.*;
import ee.sda.training.eventmanager.model.enumeration.Language;
import ee.sda.training.eventmanager.repository.*;
//import io.swagger.v3.oas.annotations.parameters.RequestBody;
import ee.sda.training.eventmanager.service.EventService;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

//import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@RequestMapping("")
public class EventRestController {

    final private EventRepository eventRepository;
    final private EventService eventService;
    final private VenueRepository venueRepository;
    final private EventTimeVenueRepository eventTimeVenueRepository;
    final private NoteRepository noteRepository;
    final private OrganisationRepository organisationRepository;
    final private EventOrganizerAndContactRepository contactRepository;
    final private PersonRepository personRepository;
    final private EventOrganizerStaffRepository staffRepository;
    final private UserRepository userRepository;
    final private AboutRepository aboutRepository;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate localDate;

    // Perhaps we need filtering by language also here
    @GetMapping("/eventapi/events")
    List<Event> listByEvent(@RequestParam(value = "language", required = false) String language,
                                  HttpServletResponse response) {

        response.addHeader("Access-Control-Allow-Origin","*");
        if (language == null) {
            return eventRepository.findAll();
        }
        else {
            return eventService.findAllByLanguage(language);
        }
    }


    @GetMapping("/eventapi/events/{id}/admin")
    Optional<Event> viewOne(@PathVariable("id") Long id,
                            HttpServletResponse response) {
        response.addHeader("Access-Control-Allow-Origin","*");
        return eventRepository.findById(id);
    }

    @GetMapping("/eventapi/events/{id}/public")
    Optional<Event> publicViewOne(@PathVariable("id") Long id,
                                  @RequestParam(value = "language", required = false) String language,
                                   HttpServletResponse response) {

        response.addHeader("Access-Control-Allow-Origin","*");
        return eventService.findByIdAndLanguage(id, language);
    }
    /**
     Events are listed by occurrences and filtered also by language (default-et)
     */

    @GetMapping("/eventapi/events/whenwhere")
    List<EventTimeVenue> listByOccurrence(@RequestParam(value = "language", required = false) String language,
                                                  HttpServletResponse response) {
        localDate = LocalDate.now();
        response.addHeader("Access-Control-Allow-Origin","*");
        return eventService.findAllUpcomingByLanguage(language, localDate);
    }

    @PostMapping("/data/api/events/add")
    String addNewEvent(@RequestBody(required = false) Event event) {

        EventOrganizerAndContact eOrgContact = new EventOrganizerAndContact();
        if(eOrgContact.getOrganisation().getId() != null) {
            Long orgId = eOrgContact.getOrganisation().getId();
            eOrgContact.setOrganisation(organisationRepository.getOne(orgId));
        }
        eOrgContact.setCreatedBy(userRepository.getOne(eOrgContact.getCreatedBy().getId()));
        eOrgContact.getEventOrganizerStaffs().forEach(staff -> {
            //staff.setEventOrganizerAndContact(eOrgContact);
            staff.setPerson(personRepository.getOne(staff.getPerson().getId()));
            //staffRepository.save(staff);
        });
        eOrgContact = contactRepository.save(eOrgContact);
        event.setEventOrganizerAndContact(eOrgContact);

        Event savedEvent = eventRepository.save(event);
        eOrgContact.setEvent(savedEvent);

        event.getEventTimeVenues().forEach(whenwhere -> {
            whenwhere.setEvent(savedEvent);
            Venue venue = venueRepository.getOne(whenwhere.getVenue().getId());
            venue.setAbouts(venue.getAbouts().stream().filter(about -> about.getLanguage() == Language.valueOf("et"))
                    .collect(Collectors.toList()));
            whenwhere.setVenue(venue);
            EventTimeVenue savedWhenWhere = eventTimeVenueRepository.save(whenwhere);
            if (whenwhere.getNotes() != null) {
                whenwhere.getNotes().forEach(note -> {
                    note.setEventTimeVenue(savedWhenWhere);
                    noteRepository.save(note);
                });
            }

        });

        event.getAbouts().forEach(about -> {
            about.setEvent(savedEvent);
            aboutRepository.save(about);
        });


        return savedEvent.getId().toString();
    }
}
